import sys
import numpy as np
distributions = []
value_indices = {}
size = 0;
for line in sys.argv[1].split("\n"):
	line=line.strip()
	nums=[int(i) for i in line.split()]
	dist = []
	for i in range(0,len(nums),2):
		value=nums[i]
		frequency=nums[i+1]
		if value in value_indices:
			index = value_indices[value]
		else:
			index = len(value_indices)
			value_indices[value] = index
		dist.extend([0 for i in range(len(dist), index)])
		dist.append(frequency)
	distributions.append(dist)

#pad all arrays to same length
longest = 0
for dist in distributions:
	l = len(dist)
	if l > longest:
		longest = l
for dist in distributions:
	dist.extend([0 for i in range(len(dist), longest)])

from scipy.stats import chi2_contingency
stat, p, dof, expected = chi2_contingency(distributions)
sys.stdout.write(str(1-p))
