use std::fs::File;
use std::io::{Read};
use crate::predictor_stats::Prediction;
use std::path::Path;
use std::num::{ParseIntError};
use crate::predictors::Predictor;
use crate::predictor_stats::Outcome::{Miss, Hit, Extraneous, Issued};
use crate::registers::NUM_REGISTERS;
use std::convert::TryInto;

pub const ERR_REACHED_END: u8 = 0;
pub const ERR_BAD_VALUE: u8 = 1;
pub const ERR_WRONG_NUMBER_OF_VALUES: u8 = 2;
pub const ERR_IO_ERROR: u8 = 3;

pub const INITIAL_LINE_LENGTH: usize = 42;
pub const BASE_LINE_LENGTH: usize = 46;
pub const REGISTERS_LENGTH: usize = 13 * (1 + NUM_REGISTERS); //target address and registers
pub const NUM_VALUES: usize = 5;

pub struct Dumpfile {
    file: File,
    steps_read: u64,
    pub(crate) line_size: u64,
    pub(crate) page_size: u64
}

impl Dumpfile {

    pub fn new(path: &Path) -> Result<Dumpfile, u8> {
        match File::open(path) {
            Ok(mut file) => {
                match Self::read_initial(&mut file) {
                    Ok((line_size, page_size)) =>
                        Ok(Dumpfile {
                            file,
                            steps_read: 0,
                            line_size,
                            page_size
                        }),
                    Err(e) =>
                        Err(e)
                }
            }
            Err(_) => {
                Err(ERR_IO_ERROR)
            }
        }
        }

    //read the first line, which contains the cacheline size and page size
    fn read_initial(file: &mut File) -> Result<(u64, u64), u8> {
        let mut line0_buf = [0u8; INITIAL_LINE_LENGTH];
        match file.read_exact(&mut line0_buf) {
            Ok(_) => {
                match String::from_utf8(line0_buf.to_vec()) {
                    Ok(string) => {
                        let values: Vec<&str> = string.split_whitespace().collect();
                        if values.len() != 2 {
                            Err(ERR_WRONG_NUMBER_OF_VALUES)
                        } else {
                            match values[0].parse::<u64>() {
                                Ok(line_size) => {
                                    match values[1].parse::<u64>() {
                                        Ok(page_size) => Ok((line_size, page_size)),
                                        Err(_) => Err(ERR_BAD_VALUE)
                                    }
                                }
                                Err(_) => Err(ERR_BAD_VALUE)
                            }
                        }
                    }
                    Err(_) => {
                        Err(ERR_BAD_VALUE)
                    }
                }
            }
            Err(_) => {
                Err(ERR_REACHED_END)
            }
        }
    }

    pub fn read_step(&mut self) -> Result<Prediction, (u8, u64)> {
        let mut buf = [0u8; BASE_LINE_LENGTH];
        match self.file.read_exact(&mut buf) {
            Ok(_) => {
                match String::from_utf8(buf.to_vec()) {
                    Ok(string) => {
                        let values: Vec<&str> = string.split_whitespace().collect();
                        if values.len() != NUM_VALUES {
                            return Err((ERR_WRONG_NUMBER_OF_VALUES, self.steps_read));
                        }
                        match Self::parse_line(values) {
                            Ok(mut prediction) => {
                                match prediction.predictor {
                                    Predictor::ACBias => {
                                        let val = self.read_target_and_registers()?;
                                        prediction.target = val.0;
                                        prediction.register_values = Option::from(val.1);
                                    },
                                    _ => {}
                                };
                                Ok(prediction)
                            }
                            Err(_parse_int_error) => {
                                Err((ERR_BAD_VALUE, self.steps_read))
                            }
                        }
                    }
                    Err(_) => {
                        Err((ERR_BAD_VALUE, self.steps_read))
                    }
                }
            }
            Err(_) => {
                Err((ERR_REACHED_END, self.steps_read))
            }
        }
    }

    fn read_target_and_registers(&mut self) -> Result<(u64, [u64; NUM_REGISTERS]), (u8, u64)> {
        let mut buf = [0u8; REGISTERS_LENGTH];
        match self.file.read_exact(&mut buf) {
            Ok(_) => {
                match String::from_utf8(buf.to_vec()) {
                    Ok(string) => {
                        let mut bad_value = false;
                        let split: Vec<&str> = string.split_whitespace().collect();
                        let target = match u64::from_str_radix(split[0], 16) {
                            Ok(val) => val,
                            Err(_) => {
                                bad_value = true;
                                0u64
                            }
                        };
                        let values: Vec<u64> = split[1..].iter().map(|string| {
                            match u64::from_str_radix(string, 16) {
                                Ok(val) => val,
                                Err(e) => {
                                    bad_value = true;
                                    0u64
                                }
                            }
                        }).collect();
                        if bad_value {
                            return Err((ERR_BAD_VALUE, self.steps_read));
                        }
                        if values.len() != NUM_REGISTERS {
                            return Err((ERR_WRONG_NUMBER_OF_VALUES, self.steps_read));
                        }
                        let boxed_array: Box<[u64; NUM_REGISTERS]> = values.into_boxed_slice().try_into().unwrap();
                        Ok((target, *boxed_array))
                    }
                    Err(_) => {
                        Err((ERR_BAD_VALUE, self.steps_read))
                    }
                }
            }
            Err(_) => {
                Err((ERR_REACHED_END, self.steps_read))
            }
        }
    }

    fn parse_line(values: Vec<&str>) -> Result<Prediction, ParseIntError> {
        Ok(Prediction {
            time_step: values[0].parse()?,
            cpu: values[1].parse()?,
            predictor: Self::parse_predictor_name(values[2])?,
            address: u64::from_str_radix(values[3], 16)?,
            hit: match values[4].parse::<u8>()? {
                0 => Miss,
                1 => Hit,
                2 => Extraneous,
                3 => Issued,
                _ => {
                    eprintln!("WARNING: Got bad outcome value when parsing dumpfile: {}", values[4]);
                    Miss
                }
            },
            target: 0u64,
            register_values: Option::None
        })
    }

    fn parse_predictor_name(name: &str) -> Result<Predictor, ParseIntError> {
        match name {
            "L1I-CA" => Ok(Predictor::L1ICache),
            "L1D-CA" => Ok(Predictor::L1DCache),
            "L2C-CA" => Ok(Predictor::L2Cache),
            "LLC-CA" => Ok(Predictor::L3Cache),
            "L1I-PF" => Ok(Predictor::L1IPrefetcher),
            "L1D-PF" => Ok(Predictor::L1DPrefetcher),
            "L2C-PF" => Ok(Predictor::L2Prefetcher),
            "LLC-PF" => Ok(Predictor::L3Prefetcher),
            "BRANCH" => Ok(Predictor::Branch),
            "ACBIAS" => Ok(Predictor::ACBias),
            _ => {
                //just a hack to get the same error type as the parse_line
                Err("abc".parse::<u8>().unwrap_err())
            }
        }
    }

    pub fn line_size(&self) -> u64 {
        self.line_size
    }

    pub fn page_size(&self) -> u64 {
        self.page_size
    }
}