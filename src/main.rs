mod predictor_stats;
mod read_dumpfile;
mod predictors;
mod registers;
mod register_bias;
mod chi2;

use gio::prelude::*;
use gtk::prelude::*;
use std::option::Option;
use crate::predictors::{Predictor, PredictorClass};
use std::rc::Rc;
use crate::read_dumpfile::{Dumpfile};
use std::path::Path;
use glib::bitflags::_core::cmp::max;
use std::borrow::{Borrow};
use std::sync::{Arc, Mutex};
use crate::predictor_stats::{Prediction, Outcome};
use crate::predictor_stats::OverallStats;
use crate::register_bias::ACBiasStats;
use gtk::Orientation;
use crate::registers::Register;
use crate::registers::REGISTERS;

const MAX_STEPS_PER_SECOND: u32 = 1000;

const RED: &str = "img/red.png";
const GREEN: &str = "img/green.png";
const YELLOW: &str = "img/yellow.png";
const GRAY: &str = "img/gray.png";
const WHITE: &str = "img/white.png";

// When the application is launched…
fn on_activate(application: &gtk::Application) {
    let glade_src = include_str!("PVT.glade");
    let builder = gtk::Builder::from_string(glade_src);

    let window: gtk::Window = builder.get_object("main_window").unwrap();
    // This needs a lock because the compiler has no way of knowing that we can't click
    // the "set file" button while the simulation is running.
    let dumpfile: Arc<Mutex<Option<Dumpfile>>> = Arc::new(Mutex::new(Option::None));
    let dumpfile_1 = dumpfile.clone();
    let dumpfile_2 = dumpfile.clone();

    let predictor_stats = Arc::new(Mutex::new(([
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
        Option::None::<OverallStats>,
    ], Option::None::<ACBiasStats>)));

    let predictor_stats_1 = predictor_stats.clone();
    let predictor_stats_2 = predictor_stats.clone();
    let predictor_stats_3 = predictor_stats.clone();
    let predictor_stats_4 = predictor_stats.clone();
    let predictor_stats_5 = predictor_stats.clone();
    let predictor_stats_6 = predictor_stats.clone();
    let predictor_stats_7 = predictor_stats.clone();
    let predictor_stats_8 = predictor_stats.clone();
    let predictor_stats_9 = predictor_stats.clone();
    let predictor_stats_10 = predictor_stats.clone();
    let predictor_stats_11 = predictor_stats.clone();
    let predictor_stats_12 = predictor_stats.clone();
    let predictor_stats_13 = predictor_stats.clone();
    let predictor_stats_14 = predictor_stats.clone();

    //What happens when we select a file -- set up all the displays based on the line/page size.
    let predictor_class_notebook: gtk::Notebook = builder.get_object("predictor_class_notebook").unwrap();
    let file_chooser: gtk::FileChooserButton = builder.get_object("open").unwrap();
    let first_button: gtk::Button = builder.get_object("first").unwrap();
    let play_button: gtk::Button = builder.get_object("play").unwrap();
    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    let next_button: gtk::Button = builder.get_object("next").unwrap();
    let steps_per_second: gtk::Entry = builder.get_object("steps_per_second").unwrap();
    let line_size_display: gtk::Label = builder.get_object("line_size").unwrap();
    let page_size_display: gtk::Label = builder.get_object("page_size").unwrap();
    let displays: [gtk::Box; 13] = [
        builder.get_object("l1ic_display").unwrap(),
        builder.get_object("l1dc_display").unwrap(),
        builder.get_object("l2c_display").unwrap(),
        builder.get_object("l3c_display").unwrap(),
        builder.get_object("l1ip_display").unwrap(),
        builder.get_object("l1dp_display").unwrap(),
        builder.get_object("l2p_display").unwrap(),
        builder.get_object("l3p_display").unwrap(),
        builder.get_object("bp1_display").unwrap(),
        builder.get_object("bp2_display").unwrap(),
        builder.get_object("bp3_display").unwrap(),
        builder.get_object("bp4_display").unwrap(),
        builder.get_object("char_display").unwrap(),
    ];

    //restrict the file chooser to .pvt files
    let filter = gtk::FileFilter::new();
    filter.set_name(Some(".pvt Files"));
    filter.add_pattern("*.pvt");
    file_chooser.add_filter(&filter);

    file_chooser.connect_file_set( move |but| {
        match but.get_filename() {
            Some(filename) => {
                match Dumpfile::new(filename.as_path()) {
                    Ok(file) => {
                        match dumpfile_1.lock() {
                            Ok(mut dumpfile_1) => {
                                *dumpfile_1 = Option::from(file);

                                //initialize overall stats array
                                match predictor_stats.lock() {
                                    Ok(mut stats_pair) => {
                                        for i in 0..9 {
                                            stats_pair.0[i] = Option::from(OverallStats::new(dumpfile_1.as_ref().unwrap().line_size, dumpfile_1.as_ref().unwrap().page_size));
                                        }
                                        stats_pair.1 = Option::from(ACBiasStats::new());
                                    },
                                    Err(_) => {
                                        panic!("Could not acquire lock to initialize predictor stats")
                                    },
                                }

                                //enable control buttons
                                first_button.set_sensitive(true);
                                play_button.set_sensitive(true);
                                pause_button.set_sensitive(true);
                                next_button.set_sensitive(true);

                                //show the user the line size and page size
                                let file = (*dumpfile_1).as_ref().unwrap();
                                line_size_display.set_label(&file.line_size().to_string());
                                page_size_display.set_label(&file.page_size().to_string());

                                //reset existing predictor display panes
                                for display in displays.iter() {
                                    //clear existing display
                                    for child in display.get_children() {
                                        display.remove(&child);
                                    }
                                }
                            }
                            Err(_) => {
                                panic!("Could not acquire lock to set dumpfile");
                            },
                        };
                    }
                    Err(err_code) => {
                        eprintln!("Got error code {}", err_code); //TODO show a dialog with the error
                    }
                }
            }
            None => {

            }
        }

        //set up display bars

    });

    let current_predictor_class = move || {
        match predictor_class_notebook.get_current_page().unwrap() {
            0 => PredictorClass::Cache,
            1 => PredictorClass::Prefetcher,
            2 => PredictorClass::Branch,
            3 => PredictorClass::ACBias,
            _ => {
                panic!("Should never get here");
            }
        }
    };

    let displays: [gtk::Box; 13] = [
        builder.get_object("l1ic_display").unwrap(),
        builder.get_object("l1dc_display").unwrap(),
        builder.get_object("l2c_display").unwrap(),
        builder.get_object("l3c_display").unwrap(),
        builder.get_object("l1ip_display").unwrap(),
        builder.get_object("l1dp_display").unwrap(),
        builder.get_object("l2p_display").unwrap(),
        builder.get_object("l3p_display").unwrap(),
        builder.get_object("bp1_display").unwrap(),
        builder.get_object("bp2_display").unwrap(),
        builder.get_object("bp3_display").unwrap(),
        builder.get_object("bp4_display").unwrap(),
        builder.get_object("char_display").unwrap()
    ];

    //this closure updates the relevant display(s) to reflect the given prediction outcome
    let display_change = move |prediction: Prediction, line_size: u64, page_size: u64, which_bp_displays: [bool; 4], bias: Option<(Register, f64)>| {
        //we need this strange syntax because branch prediction outcomes could affect multiple display panes
        let relevant_displays: Vec<&gtk::Box> = match prediction.predictor {
            Predictor::L1ICache => {vec![&displays[0]]},
            Predictor::L1DCache => {vec![&displays[1]]},
            Predictor::L2Cache => {vec![&displays[2]]},
            Predictor::L3Cache => {vec![&displays[3]]},
            Predictor::L1IPrefetcher => {vec![&displays[4]]},
            Predictor::L1DPrefetcher => {vec![&displays[5]]},
            Predictor::L2Prefetcher => {vec![&displays[6]]},
            Predictor::L3Prefetcher => {vec![&displays[7]]},
            Predictor::Branch => {
                let mut vec = vec![];
                for i in 0..which_bp_displays.len() {
                    if which_bp_displays[i] {
                        vec.push(&displays[8 + i]);
                    }
                }
                vec
            },
            Predictor::ACBias => {vec![&displays[12]]}
        };
        for display in relevant_displays {
            let mut found_row = false;
            let mut display_row_children;
            let color = Path::new(match prediction.predictor {
                Predictor::ACBias => {
                    GREEN
                },
                _=> {
                    match prediction.hit {
                        Outcome::Miss => { RED },
                        Outcome::Hit => { GREEN },
                        Outcome::Extraneous => { YELLOW },
                        Outcome::Issued => { GRAY },
                    }
                }
            });
            for display_row in display.get_children() {
                let display_row = display_row.downcast::<gtk::Box>().unwrap();

                //Get the address from the row label
                //We get the label like this...
                display_row_children = display_row.get_children();
                let label: &gtk::Label = display_row_children.get(0).unwrap().downcast_ref::<gtk::Label>().unwrap();
                //...and then parse the hex string, ignoring the '0x'.
                let row_page_address = u64::from_str_radix(&label.get_label().to_string()[2..], 16).unwrap();
                //if we find the display row corresponding to the memory page we want, flag it as the one to update
                if prediction.address - (prediction.address % page_size) == row_page_address {
                    found_row = true;

                    //set the icon corresponding to the right cacheline to red or green for a miss or a hit, respectively.
                    let line_in_page = ((prediction.address % page_size) / line_size) as usize;
                    let display_row_slots = display_row_children.get(1).unwrap().downcast_ref::<gtk::Box>().unwrap();
                    let stacks = display_row_slots.get_children();
                    let stack = stacks[line_in_page].downcast_ref::<gtk::Box>().unwrap();
                    let elements = stack.get_children();
                    let label = elements[0].downcast_ref::<gtk::Label>().unwrap();
                    let icon = elements[1].downcast_ref::<gtk::Image>().unwrap();
                    icon.set_from_file(color);
                    match bias {
                        Some(bias) => {
                            icon.set_opacity(bias.1);
                            label.set_text(bias.0.to_str());
                        }
                        None => {}
                    }
                    stack.show_all();
                    break;
                }
            }
            if !found_row {
                //we didn't find a row for the right page, so we have to add it and keep the rows sorted.
                let new_display_row = gtk::Box::new(Orientation::Horizontal, 10);

                //page address label
                let row_label = gtk::Label::new(Option::from(format!("{:#012x}", prediction.address - (prediction.address % page_size)).as_ref()));

                //cacheline status icons
                let inner_display_row = gtk::Box::new(Orientation::Horizontal, 0);
                //add a "button" for each cacheline within the page
                //effectively ceil(page_size / line_size) without truncation
                let display_row_entries = ((page_size - 1) / line_size + 1) as i32;
                for i in 0..display_row_entries {
                    let stack = gtk::Box::new(Orientation::Vertical, 2);
                    let icon = gtk::Image::from_file(&Path::new(WHITE));
                    let label = gtk::Label::new(Option::from(""));
                    label.set_opacity(0.5);
                    stack.add(&label);
                    stack.add(&icon);
                    icon.set_hexpand(true);
                    stack.set_hexpand(true);
                    inner_display_row.add(&stack);
                }
                new_display_row.add(&row_label);
                new_display_row.add(&inner_display_row);

                display.add(&new_display_row);
                //put the row in the right position based on the address
                //first determine how many rows have an address less than the new one
                let mut less_than_current: usize = 0;
                let display_rows = display.get_children();
                for i in (0..display.get_children().len() - 1).rev() {
                    let existing_row = display_rows.get(i).unwrap();
                    let row_halves = existing_row.downcast_ref::<gtk::Box>().unwrap().get_children();
                    let label = row_halves
                        .get(0).unwrap().downcast_ref::<gtk::Label>().unwrap();
                    let other_address = u64::from_str_radix(&label.get_label()[2..], 16).unwrap();
                    if prediction.address >= other_address {
                        break;
                    } else {
                        /*
                            We have to reorder one step at a time. If we just found the right place
                            and moved it once, GTK would just swap the new row wit the one at the
                            final position, and the order would be messed up.
                        */
                        display.reorder_child(&new_display_row, i as i32);
                    }
                }
                //set the icon corresponding to the right cacheline to red or green for a miss or a hit, respectively.
                let line_in_page = ((prediction.address % page_size) / line_size) as usize;
                let display_row_slots = inner_display_row;
                let stacks = display_row_slots.get_children();
                let stack = stacks[line_in_page].downcast_ref::<gtk::Box>().unwrap();
                let elements = stack.get_children();
                let label = elements[0].downcast_ref::<gtk::Label>().unwrap();
                let icon = elements[1].downcast_ref::<gtk::Image>().unwrap();
                icon.set_from_file(color);
                match bias {
                    Some(bias) => {
                        icon.set_opacity(bias.1);
                        label.set_text(bias.0.to_str());
                    }
                    None => {}
                }
                new_display_row.show_all();
            }
        }
    };

    let paused: Arc<Mutex<bool>> = Arc::new(Mutex::new(true));
    let paused_1 = paused.clone();
    let paused_2 = paused.clone();
    let paused_3 = paused.clone();
    let paused_4 = paused.clone();

    let file_chooser: gtk::FileChooserButton = builder.get_object("open").unwrap();
    let first_button: gtk::Button = builder.get_object("first").unwrap();
    let play_button: gtk::Button = builder.get_object("play").unwrap();
    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    let next_button: gtk::Button = builder.get_object("next").unwrap();

    let l1ic_min_addr: gtk::Entry = builder.get_object("l1ic_min_addr").unwrap();
    let l1ic_max_addr: gtk::Entry = builder.get_object("l1ic_max_addr").unwrap();
    let l1dc_min_addr: gtk::Entry = builder.get_object("l1dc_min_addr").unwrap();
    let l1dc_max_addr: gtk::Entry = builder.get_object("l1dc_max_addr").unwrap();
    let l2c_min_addr: gtk::Entry = builder.get_object("l2c_min_addr").unwrap();
    let l2c_max_addr: gtk::Entry = builder.get_object("l2c_max_addr").unwrap();
    let l3c_min_addr: gtk::Entry = builder.get_object("l3c_min_addr").unwrap();
    let l3c_max_addr: gtk::Entry = builder.get_object("l3c_max_addr").unwrap();
    let l1ip_min_addr: gtk::Entry = builder.get_object("l1ip_min_addr").unwrap();
    let l1ip_max_addr: gtk::Entry = builder.get_object("l1ip_max_addr").unwrap();
    let l1dp_min_addr: gtk::Entry = builder.get_object("l1dp_min_addr").unwrap();
    let l1dp_max_addr: gtk::Entry = builder.get_object("l1dp_max_addr").unwrap();
    let l2p_min_addr: gtk::Entry = builder.get_object("l2p_min_addr").unwrap();
    let l2p_max_addr: gtk::Entry = builder.get_object("l2p_max_addr").unwrap();
    let l3p_min_addr: gtk::Entry = builder.get_object("l3p_min_addr").unwrap();
    let l3p_max_addr: gtk::Entry = builder.get_object("l3p_max_addr").unwrap();
    let bp1_min_addr: gtk::Entry = builder.get_object("bp1_min_addr").unwrap();
    let bp1_max_addr: gtk::Entry = builder.get_object("bp1_max_addr").unwrap();
    let bp2_min_addr: gtk::Entry = builder.get_object("bp2_min_addr").unwrap();
    let bp2_max_addr: gtk::Entry = builder.get_object("bp2_max_addr").unwrap();
    let bp3_min_addr: gtk::Entry = builder.get_object("bp3_min_addr").unwrap();
    let bp3_max_addr: gtk::Entry = builder.get_object("bp3_max_addr").unwrap();
    let bp4_min_addr: gtk::Entry = builder.get_object("bp4_min_addr").unwrap();
    let bp4_max_addr: gtk::Entry = builder.get_object("bp4_max_addr").unwrap();
    let l1ic_cpus: gtk::Box = builder.get_object("l1ic_cpus").unwrap();
    let l1dc_cpus: gtk::Box = builder.get_object("l1dc_cpus").unwrap();
    let l2c_cpus: gtk::Box = builder.get_object("l2c_cpus").unwrap();
    let l1ip_cpus: gtk::Box = builder.get_object("l1ip_cpus").unwrap();
    let l1dp_cpus: gtk::Box = builder.get_object("l1dp_cpus").unwrap();
    let l2p_cpus: gtk::Box = builder.get_object("l2p_cpus").unwrap();
    let bp1_cpus: gtk::Box = builder.get_object("bp1_cpus").unwrap();
    let bp2_cpus: gtk::Box = builder.get_object("bp2_cpus").unwrap();
    let bp3_cpus: gtk::Box = builder.get_object("bp3_cpus").unwrap();
    let bp4_cpus: gtk::Box = builder.get_object("bp4_cpus").unwrap();

    let parse_address = |mut string: &str, lower_bound: bool| -> u64 {
        if string.len() >= 2 && string[0..2].eq("0x") {
            string = &string[2..];
        }
        match u64::from_str_radix(string, 16) {
            Ok(val) => val,
            Err(_) => if lower_bound { 0 } else { 0xFFFFFFFFFFFFFFFF }
        }
    };
    let check_cpu = |visual_cpu_block: &gtk::Box, cpu: u8| -> bool {
        let rows = visual_cpu_block.get_children();
        let row_1 = rows.get(0).unwrap().downcast_ref::<gtk::Box>().unwrap();
        let row_2 = rows.get(1).unwrap().downcast_ref::<gtk::Box>().unwrap();
        let row_1_check_boxes = row_1.get_children();
        let row_2_check_boxes = row_2.get_children();
        let cpu_check_boxes = [
            row_1_check_boxes.get(0).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(1).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(2).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(3).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(4).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(5).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(6).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_1_check_boxes.get(7).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(0).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(1).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(2).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(3).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(4).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(5).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(6).unwrap().downcast_ref::<gtk::CheckButton>().unwrap(),
            row_2_check_boxes.get(7).unwrap().downcast_ref::<gtk::CheckButton>().unwrap()
        ];
        cpu_check_boxes[cpu as usize].get_active()
    };

    //logic for updating stats in left pane
    let l1ic_min_addr: gtk::Entry = builder.get_object("l1ic_min_addr").unwrap();
    let l1ic_max_addr: gtk::Entry = builder.get_object("l1ic_max_addr").unwrap();
    let l1dc_min_addr: gtk::Entry = builder.get_object("l1dc_min_addr").unwrap();
    let l1dc_max_addr: gtk::Entry = builder.get_object("l1dc_max_addr").unwrap();
    let l2c_min_addr: gtk::Entry = builder.get_object("l2c_min_addr").unwrap();
    let l2c_max_addr: gtk::Entry = builder.get_object("l2c_max_addr").unwrap();
    let l3c_min_addr: gtk::Entry = builder.get_object("l3c_min_addr").unwrap();
    let l3c_max_addr: gtk::Entry = builder.get_object("l3c_max_addr").unwrap();
    let l1ip_min_addr: gtk::Entry = builder.get_object("l1ip_min_addr").unwrap();
    let l1ip_max_addr: gtk::Entry = builder.get_object("l1ip_max_addr").unwrap();
    let l1dp_min_addr: gtk::Entry = builder.get_object("l1dp_min_addr").unwrap();
    let l1dp_max_addr: gtk::Entry = builder.get_object("l1dp_max_addr").unwrap();
    let l2p_min_addr: gtk::Entry = builder.get_object("l2p_min_addr").unwrap();
    let l2p_max_addr: gtk::Entry = builder.get_object("l2p_max_addr").unwrap();
    let l3p_min_addr: gtk::Entry = builder.get_object("l3p_min_addr").unwrap();
    let l3p_max_addr: gtk::Entry = builder.get_object("l3p_max_addr").unwrap();
    let bp1_min_addr: gtk::Entry = builder.get_object("bp1_min_addr").unwrap();
    let bp1_max_addr: gtk::Entry = builder.get_object("bp1_max_addr").unwrap();
    let bp2_min_addr: gtk::Entry = builder.get_object("bp2_min_addr").unwrap();
    let bp2_max_addr: gtk::Entry = builder.get_object("bp2_max_addr").unwrap();
    let bp3_min_addr: gtk::Entry = builder.get_object("bp3_min_addr").unwrap();
    let bp3_max_addr: gtk::Entry = builder.get_object("bp3_max_addr").unwrap();
    let bp4_min_addr: gtk::Entry = builder.get_object("bp4_min_addr").unwrap();
    let bp4_max_addr: gtk::Entry = builder.get_object("bp4_max_addr").unwrap();
    let l1ic_cpus: gtk::Box = builder.get_object("l1ic_cpus").unwrap();
    let l1dc_cpus: gtk::Box = builder.get_object("l1dc_cpus").unwrap();
    let l2c_cpus: gtk::Box = builder.get_object("l2c_cpus").unwrap();
    let l1ip_cpus: gtk::Box = builder.get_object("l1ip_cpus").unwrap();
    let l1dp_cpus: gtk::Box = builder.get_object("l1dp_cpus").unwrap();
    let l2p_cpus: gtk::Box = builder.get_object("l2p_cpus").unwrap();
    let bp1_cpus: gtk::Box = builder.get_object("bp1_cpus").unwrap();
    let bp2_cpus: gtk::Box = builder.get_object("bp2_cpus").unwrap();
    let bp3_cpus: gtk::Box = builder.get_object("bp3_cpus").unwrap();
    let bp4_cpus: gtk::Box = builder.get_object("bp4_cpus").unwrap();

    let l1ic_accesses: gtk::Label = builder.get_object("l1ic_accesses").unwrap();
    let l1ic_hits: gtk::Label = builder.get_object("l1ic_hits").unwrap();
    let l1ic_rate: gtk::Label = builder.get_object("l1ic_rate").unwrap();
    let l1dc_accesses: gtk::Label = builder.get_object("l1dc_accesses").unwrap();
    let l1dc_hits: gtk::Label = builder.get_object("l1dc_hits").unwrap();
    let l1dc_rate: gtk::Label = builder.get_object("l1dc_rate").unwrap();
    let l2c_accesses: gtk::Label = builder.get_object("l2c_accesses").unwrap();
    let l2c_hits: gtk::Label = builder.get_object("l2c_hits").unwrap();
    let l2c_rate: gtk::Label = builder.get_object("l2c_rate").unwrap();
    let l3c_accesses: gtk::Label = builder.get_object("l3c_accesses").unwrap();
    let l3c_hits: gtk::Label = builder.get_object("l3c_hits").unwrap();
    let l3c_rate: gtk::Label = builder.get_object("l3c_rate").unwrap();
    let l1ip_accesses: gtk::Label = builder.get_object("l1ip_accesses").unwrap();
    let l1ip_hits: gtk::Label = builder.get_object("l1ip_hits").unwrap();
    let l1ip_rate: gtk::Label = builder.get_object("l1ip_rate").unwrap();
    let l1ip_issued: gtk::Label = builder.get_object("l1ip_issued").unwrap();
    let l1ip_extraneous: gtk::Label = builder.get_object("l1ip_extraneous").unwrap();
    let l1ip_accuracy: gtk::Label = builder.get_object("l1ip_accuracy").unwrap();
    let l1dp_accesses: gtk::Label = builder.get_object("l1dp_accesses").unwrap();
    let l1dp_hits: gtk::Label = builder.get_object("l1dp_hits").unwrap();
    let l1dp_rate: gtk::Label = builder.get_object("l1dp_rate").unwrap();
    let l1dp_issued: gtk::Label = builder.get_object("l1dp_issued").unwrap();
    let l1dp_extraneous: gtk::Label = builder.get_object("l1dp_extraneous").unwrap();
    let l1dp_accuracy: gtk::Label = builder.get_object("l1dp_accuracy").unwrap();
    let l2p_accesses: gtk::Label = builder.get_object("l2p_accesses").unwrap();
    let l2p_hits: gtk::Label = builder.get_object("l2p_hits").unwrap();
    let l2p_rate: gtk::Label = builder.get_object("l2p_rate").unwrap();
    let l2p_issued: gtk::Label = builder.get_object("l2p_issued").unwrap();
    let l2p_extraneous: gtk::Label = builder.get_object("l2p_extraneous").unwrap();
    let l2p_accuracy: gtk::Label = builder.get_object("l2p_accuracy").unwrap();
    let l3p_accesses: gtk::Label = builder.get_object("l3p_accesses").unwrap();
    let l3p_hits: gtk::Label = builder.get_object("l3p_hits").unwrap();
    let l3p_rate: gtk::Label = builder.get_object("l3p_rate").unwrap();
    let l3p_issued: gtk::Label = builder.get_object("l3p_issued").unwrap();
    let l3p_extraneous: gtk::Label = builder.get_object("l3p_extraneous").unwrap();
    let l3p_accuracy: gtk::Label = builder.get_object("l3p_accuracy").unwrap();
    let bp1_accesses: gtk::Label = builder.get_object("bp1_accesses").unwrap();
    let bp1_hits: gtk::Label = builder.get_object("bp1_hits").unwrap();
    let bp1_rate: gtk::Label = builder.get_object("bp1_rate").unwrap();
    let bp2_accesses: gtk::Label = builder.get_object("bp2_accesses").unwrap();
    let bp2_hits: gtk::Label = builder.get_object("bp2_hits").unwrap();
    let bp2_rate: gtk::Label = builder.get_object("bp2_rate").unwrap();
    let bp3_accesses: gtk::Label = builder.get_object("bp3_accesses").unwrap();
    let bp3_hits: gtk::Label = builder.get_object("bp3_hits").unwrap();
    let bp3_rate: gtk::Label = builder.get_object("bp3_rate").unwrap();
    let bp4_accesses: gtk::Label = builder.get_object("bp4_accesses").unwrap();
    let bp4_hits: gtk::Label = builder.get_object("bp4_hits").unwrap();
    let bp4_rate: gtk::Label = builder.get_object("bp4_rate").unwrap();

    // (min addr, max addr, cpus, accesses, hits, rate/coverage, issued, extraneous, accuracy)
    let l1ic_controls = (l1ic_min_addr, l1ic_max_addr, Option::from(l1ic_cpus), l1ic_accesses, l1ic_hits, l1ic_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1dc_controls = (l1dc_min_addr, l1dc_max_addr, Option::from(l1dc_cpus), l1dc_accesses, l1dc_hits, l1dc_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l2c_controls = (l2c_min_addr, l2c_max_addr, Option::from(l2c_cpus), l2c_accesses, l2c_hits, l2c_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l3c_controls = (l3c_min_addr, l3c_max_addr, Option::None::<gtk::Box>, l3c_accesses, l3c_hits, l3c_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1ip_controls = (l1ip_min_addr, l1ip_max_addr, Option::from(l1ip_cpus), l1ip_accesses, l1ip_hits, l1ip_rate, Option::from(l1ip_issued), Option::from(l1ip_extraneous), Option::from(l1ip_accuracy));
    let l1dp_controls = (l1dp_min_addr, l1dp_max_addr, Option::from(l1dp_cpus), l1dp_accesses, l1dp_hits, l1dp_rate, Option::from(l1dp_issued), Option::from(l1dp_extraneous), Option::from(l1dp_accuracy));
    let l2p_controls = (l2p_min_addr, l2p_max_addr, Option::from(l2p_cpus), l2p_accesses, l2p_hits, l2p_rate, Option::from(l2p_issued), Option::from(l2p_extraneous), Option::from(l2p_accuracy));
    let l3p_controls = (l3p_min_addr, l3p_max_addr, Option::None::<gtk::Box>, l3p_accesses, l3p_hits, l3p_rate, Option::from(l3p_issued), Option::from(l3p_extraneous), Option::from(l3p_accuracy));
    let bp1_controls = (bp1_min_addr, bp1_max_addr, Option::from(bp1_cpus), bp1_accesses, bp1_hits, bp1_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp2_controls = (bp2_min_addr, bp2_max_addr, Option::from(bp2_cpus), bp2_accesses, bp2_hits, bp2_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp3_controls = (bp3_min_addr, bp3_max_addr, Option::from(bp3_cpus), bp3_accesses, bp3_hits, bp3_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp4_controls = (bp4_min_addr, bp4_max_addr, Option::from(bp4_cpus), bp4_accesses, bp4_hits, bp4_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);

    let update_stats_for_filter = move |overall_stats: &OverallStats, controls: &(gtk::Entry, gtk::Entry, Option<gtk::Box>, gtk::Label, gtk::Label, gtk::Label, Option<gtk::Label>, Option<gtk::Label>, Option<gtk::Label>)| {
        let min_addr = &controls.0;
        let max_addr = &controls.1;
        let opt_cpus = &controls.2;
        let accesses = &controls.3;
        let hits = &controls.4;
        let rate = &controls.5;
        let opt_issued = &controls.6;
        let opt_extraneous = &controls.7;
        let opt_accuracy = &controls.8;


        let cpus= match opt_cpus {
            Some(cpu_display) => {
                [
                    check_cpu(&cpu_display, 0),
                    check_cpu(&cpu_display, 1),
                    check_cpu(&cpu_display, 2),
                    check_cpu(&cpu_display, 3),
                    check_cpu(&cpu_display, 4),
                    check_cpu(&cpu_display, 5),
                    check_cpu(&cpu_display, 6),
                    check_cpu(&cpu_display, 7),
                    check_cpu(&cpu_display, 8),
                    check_cpu(&cpu_display, 9),
                    check_cpu(&cpu_display, 10),
                    check_cpu(&cpu_display, 11),
                    check_cpu(&cpu_display, 12),
                    check_cpu(&cpu_display, 13),
                    check_cpu(&cpu_display, 14),
                    check_cpu(&cpu_display, 15),
                ]
            },
            None => {
                //L3 Cache/Prefetcher don't have cpu-specific events, so just mark them all as selected by default
                [true; 16]
            },
        };

        let overall_stats = overall_stats.get_overall_stats(parse_address(&min_addr.get_text().to_string(), true)..
                                                                parse_address(&max_addr.get_text().to_string(), false), cpus);

        hits.set_text(&overall_stats.0.to_string());
        accesses.set_text(&overall_stats.1.to_string());
        let rate_val = if overall_stats.1 != 0 {
            (overall_stats.0 as f64) / (overall_stats.1 as f64)
        } else { 0.0f64 };
        rate.set_text(format!("{:.2}%", rate_val * 100.0).as_ref());
        match opt_issued {
            Some(issued) => {
                issued.set_text(&overall_stats.3.to_string());
            }
            None => {}
        }
        match opt_extraneous {
            Some(extraneous) => {
                extraneous.set_text(&overall_stats.2.to_string());
            }
            None => {}
        }
        match opt_accuracy {
            Some(accuracy) => {
                let accuracy_val = if overall_stats.2 != 0 {
                    (overall_stats.0 as f64) / (overall_stats.2 as f64)
                } else {
                    0.0f64
                };
                accuracy.set_text(format!("{:.2}%", accuracy_val * 100.0).as_ref());
            }
            None => {}
        }
    };

    let l1ic_min_addr: gtk::Entry = builder.get_object("l1ic_min_addr").unwrap();
    let l1ic_max_addr: gtk::Entry = builder.get_object("l1ic_max_addr").unwrap();
    let l1dc_min_addr: gtk::Entry = builder.get_object("l1dc_min_addr").unwrap();
    let l1dc_max_addr: gtk::Entry = builder.get_object("l1dc_max_addr").unwrap();
    let l2c_min_addr: gtk::Entry = builder.get_object("l2c_min_addr").unwrap();
    let l2c_max_addr: gtk::Entry = builder.get_object("l2c_max_addr").unwrap();
    let l3c_min_addr: gtk::Entry = builder.get_object("l3c_min_addr").unwrap();
    let l3c_max_addr: gtk::Entry = builder.get_object("l3c_max_addr").unwrap();
    let l1ip_min_addr: gtk::Entry = builder.get_object("l1ip_min_addr").unwrap();
    let l1ip_max_addr: gtk::Entry = builder.get_object("l1ip_max_addr").unwrap();
    let l1dp_min_addr: gtk::Entry = builder.get_object("l1dp_min_addr").unwrap();
    let l1dp_max_addr: gtk::Entry = builder.get_object("l1dp_max_addr").unwrap();
    let l2p_min_addr: gtk::Entry = builder.get_object("l2p_min_addr").unwrap();
    let l2p_max_addr: gtk::Entry = builder.get_object("l2p_max_addr").unwrap();
    let l3p_min_addr: gtk::Entry = builder.get_object("l3p_min_addr").unwrap();
    let l3p_max_addr: gtk::Entry = builder.get_object("l3p_max_addr").unwrap();
    let bp1_min_addr: gtk::Entry = builder.get_object("bp1_min_addr").unwrap();
    let bp1_max_addr: gtk::Entry = builder.get_object("bp1_max_addr").unwrap();
    let bp2_min_addr: gtk::Entry = builder.get_object("bp2_min_addr").unwrap();
    let bp2_max_addr: gtk::Entry = builder.get_object("bp2_max_addr").unwrap();
    let bp3_min_addr: gtk::Entry = builder.get_object("bp3_min_addr").unwrap();
    let bp3_max_addr: gtk::Entry = builder.get_object("bp3_max_addr").unwrap();
    let bp4_min_addr: gtk::Entry = builder.get_object("bp4_min_addr").unwrap();
    let bp4_max_addr: gtk::Entry = builder.get_object("bp4_max_addr").unwrap();
    let char_min_addr: gtk::Entry = builder.get_object("char_min_addr").unwrap();
    let char_max_addr: gtk::Entry = builder.get_object("char_max_addr").unwrap();
    let l1ic_cpus: gtk::Box = builder.get_object("l1ic_cpus").unwrap();
    let l1dc_cpus: gtk::Box = builder.get_object("l1dc_cpus").unwrap();
    let l2c_cpus: gtk::Box = builder.get_object("l2c_cpus").unwrap();
    let l1ip_cpus: gtk::Box = builder.get_object("l1ip_cpus").unwrap();
    let l1dp_cpus: gtk::Box = builder.get_object("l1dp_cpus").unwrap();
    let l2p_cpus: gtk::Box = builder.get_object("l2p_cpus").unwrap();
    let bp1_cpus: gtk::Box = builder.get_object("bp1_cpus").unwrap();
    let bp2_cpus: gtk::Box = builder.get_object("bp2_cpus").unwrap();
    let bp3_cpus: gtk::Box = builder.get_object("bp3_cpus").unwrap();
    let bp4_cpus: gtk::Box = builder.get_object("bp4_cpus").unwrap();
    let char_cpus: gtk::Box = builder.get_object("char_cpus").unwrap();

    let char_registers: gtk::Box = builder.get_object("char_regs");

    /*
        perform_step updates the GUI to reflect as many dumpfile predictions as needed in order for
        a single new prediction to become visible on the GUI (i.e. a single new hit or miss displays
        for the predictor type selected, within the cpu/address range specified for the relevant
        predictor)
    */
    let perform_step = Rc::new(move || {
        //println!("perform_step() executed, {} ms elapsed.", 0);
        match dumpfile.lock() {
            Ok(mut mtx_dumpfile) => {
              match mtx_dumpfile.as_mut() {
                  Some(dumpfile) => {
                      let mut matches_filter = false;
                      let mut correct_pane = false;
                      while !(matches_filter && correct_pane) {
                          let selected_class = current_predictor_class();
                          matches_filter = false;
                          correct_pane = false;
                          let mut which_bp_displays = [false; 4];
                          match dumpfile.read_step() {
                              Ok(prediction) => {
                                  /*
                                    We always submit this prediction for GUI update if it matches the filter,
                                    but keep looping until we get one that will show a visual update on the current panel.
                                  */
                                  match prediction.predictor {
                                      Predictor::L1ICache => {
                                          match selected_class {
                                              PredictorClass::Cache => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l1ic_min_addr.get_text().to_string(), true)..
                                              parse_address(&l1ic_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l1ic_cpus, prediction.cpu);
                                      },
                                      Predictor::L1DCache => {
                                          match selected_class {
                                              PredictorClass::Cache => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l1dc_min_addr.get_text().to_string(), true)..
                                              parse_address(&l1dc_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l1dc_cpus, prediction.cpu);
                                      },
                                      Predictor::L2Cache => {
                                          match selected_class {
                                              PredictorClass::Cache => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l2c_min_addr.get_text().to_string(), true)..
                                              parse_address(&l2c_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l2c_cpus, prediction.cpu);
                                      },
                                      Predictor::L3Cache => {
                                          match selected_class {
                                              PredictorClass::Cache => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          //no check for cpus on L3, since it's shared
                                          matches_filter = (parse_address(&l3c_min_addr.get_text().to_string(), true)..
                                              parse_address(&l3c_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address);
                                      },
                                      Predictor::L1IPrefetcher => {
                                          match selected_class {
                                              PredictorClass::Prefetcher => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l1ip_min_addr.get_text().to_string(), true)..
                                              parse_address(&l1ip_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l1ip_cpus, prediction.cpu);
                                      },
                                      Predictor::L1DPrefetcher => {
                                          match selected_class {
                                              PredictorClass::Prefetcher => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l1dp_min_addr.get_text().to_string(), true)..
                                              parse_address(&l1dp_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l1dp_cpus, prediction.cpu);
                                      },
                                      Predictor::L2Prefetcher => {
                                          match selected_class {
                                              PredictorClass::Prefetcher => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          matches_filter = (parse_address(&l2p_min_addr.get_text().to_string(), true)..
                                              parse_address(&l2p_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&l2p_cpus, prediction.cpu);
                                      },
                                      Predictor::L3Prefetcher => {
                                          match selected_class {
                                              PredictorClass::Prefetcher => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          //again, no cpus on L3
                                          matches_filter = (parse_address(&l3p_min_addr.get_text().to_string(), true)..
                                              parse_address(&l3p_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address);
                                      },
                                      Predictor::Branch => {
                                          match selected_class {
                                              PredictorClass::Branch => {
                                                  correct_pane = true;
                                              },
                                              _=>{}
                                          }
                                          //an update on any of the branch prediction displays counts as a visual change
                                          which_bp_displays[0] = (parse_address(&bp1_min_addr.get_text().to_string(), true)..
                                              parse_address(&bp1_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&bp1_cpus, prediction.cpu);
                                          which_bp_displays[1] =(parse_address(&bp2_min_addr.get_text().to_string(), true)..
                                              parse_address(&bp2_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&bp2_cpus, prediction.cpu);
                                          which_bp_displays[2] = (parse_address(&bp3_min_addr.get_text().to_string(), true)..
                                              parse_address(&bp3_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&bp3_cpus, prediction.cpu);
                                          which_bp_displays[3] = (parse_address(&bp4_min_addr.get_text().to_string(), true)..
                                              parse_address(&bp4_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&bp4_cpus, prediction.cpu);
                                          matches_filter = which_bp_displays[0] || which_bp_displays[1]
                                              || which_bp_displays[2] || which_bp_displays[3];
                                      },
                                      Predictor::ACBias => {
                                          match selected_class {
                                              PredictorClass::ACBias => {
                                                  correct_pane = true;
                                              },
                                              _=> {}
                                          }
                                          matches_filter = (parse_address(&char_min_addr.get_text().to_string(), true)..
                                              parse_address(&char_max_addr.get_text().to_string(), false))
                                              .contains(&prediction.address) &&
                                              check_cpu(&char_cpus, prediction.cpu);
                                      }
                                  };
                                  //update the stats and update the left pane to reflect this
                                  let mut all_stats = predictor_stats_1.lock().expect("Could not acquire lock to update predictor stats.");
                                  let stats_array = &mut all_stats.0;
                                  let mut placeholder = Option::None::<OverallStats>;
                                  let opt_my_stats = match prediction.predictor {
                                      Predictor::L1ICache => {&mut stats_array[0]},
                                      Predictor::L1DCache => {&mut stats_array[1]},
                                      Predictor::L2Cache => {&mut stats_array[2]},
                                      Predictor::L3Cache => {&mut stats_array[3]},
                                      Predictor::L1IPrefetcher => {&mut stats_array[4]},
                                      Predictor::L1DPrefetcher => {&mut stats_array[5]},
                                      Predictor::L2Prefetcher => {&mut stats_array[6]},
                                      Predictor::L3Prefetcher => {&mut stats_array[7]},
                                      Predictor::Branch => {&mut stats_array[8]},
                                      Predictor::ACBias => {&mut placeholder} //ACBias doesn't have an OverallStats, so we handle it separately
                                  };
                                  let gui_controls = match prediction.predictor {
                                      Predictor::L1ICache => {vec![&l1ic_controls]},
                                      Predictor::L1DCache => {vec![&l1dc_controls]},
                                      Predictor::L2Cache => {vec![&l2c_controls]},
                                      Predictor::L3Cache => {vec![&l3c_controls]},
                                      Predictor::L1IPrefetcher => {vec![&l1ip_controls]},
                                      Predictor::L1DPrefetcher => {vec![&l1dp_controls]},
                                      Predictor::L2Prefetcher => {vec![&l2p_controls]},
                                      Predictor::L3Prefetcher => {vec![&l3p_controls]},
                                      Predictor::Branch => {vec![&bp1_controls, &bp2_controls, &bp3_controls, &bp4_controls]},
                                      Predictor::ACBias => {vec![]}
                                  };
                                  let mut bias = Option::None;
                                  match prediction.predictor {
                                      Predictor::ACBias => {
                                          match &mut all_stats.1 {
                                              Some(bias_stats) => {
                                                  bias_stats.put(prediction.address, prediction.target, prediction.register_values.expect("Register values not set for ACBias result"));
                                                  //find the best bias register
                                                  let mut best_fit = (Register::RIP, 0.0f64);
                                                  for r in REGISTERS.iter() {
                                                      let fit = (*r, bias_stats.trigger_bias(*r, prediction.address));
                                                      if fit.1 > best_fit.1 {
                                                          best_fit = fit;
                                                      }
                                                  }
                                                  bias = Option::from(best_fit);
                                              }
                                              None => {
                                                  //we could reasonably get here if the user clicks the 'first' button.
                                                  return false;
                                              }
                                          }
                                      },
                                      _=> {
                                          match opt_my_stats.as_mut() {
                                              Some(my_stats) => {
                                                  my_stats.put(prediction.address, prediction.cpu as usize, prediction.hit);
                                                  for controls_set in gui_controls {
                                                      update_stats_for_filter(&my_stats, controls_set);
                                                  }
                                              },
                                              None => {
                                                  //we could reasonably get here if the user clicks the 'first' button.
                                                  return false;
                                              },
                                          }
                                      }
                                  }
                                  // Display the change. This closure does not check the filters,
                                  // so we should only call it if we know the change should be displayed.
                                  if matches_filter {
                                      display_change(prediction, dumpfile.line_size, dumpfile.page_size, which_bp_displays, bias)
                                  }
                              },
                              Err((err_code, line)) => {
                                  match err_code {
                                      err_reached_end => {
                                          /*
                                            If we reach the end of the file, pause and disable the play/pause/next
                                            buttons, and enable the file chooser button.
                                          */
                                          play_button.set_sensitive(false);
                                          pause_button.set_sensitive(false);
                                          next_button.set_sensitive(false);
                                          file_chooser.set_sensitive(true);
                                          //we might have never had any visual changes before we reached the end, so break out early and mark as need to pause
                                          return false;
                                      }
                                      err_bad_value => {
                                          eprintln!("ERROR: Got bad data from dumpfile on line {}", line)
                                      }
                                      err_wrong_value_count => {
                                          eprintln!("ERROR: Read the wrong number of values from the dumpfile on line {}", line)
                                      }
                                      err_io_error => {
                                          eprintln!("ERROR: Received a IO error reading the dumpfile on line {}", line)
                                      }
                                  }
                              }
                          }
                      }
                  }
                  None => {
                      eprintln!("ERROR: attempted to perform step with no dumpfile selected.");
                  }
              }
            },
            Err(_) => {
                panic!("Could not acquire lock for dumpfile to perform step.")
            },
        }
        /*let mtx: &Mutex<gtk::Window> = window_1.borrow();
        match mtx.lock() {
            Ok(window) => {
                window.show_all();
            }
            Err(_) => {
                panic!("Could not acquire lock to redraw screen");
            }
        };*/
        true
    });
    let perform_step_1 = perform_step.clone();

    let next_button: gtk::Button = builder.get_object("next").unwrap();
    next_button.connect_clicked( move |but| {
        perform_step();
    });

    let get_steps_per_second = move || {
        let text = steps_per_second.get_text().to_string();
        match text.parse::<u32>() {
            Ok(val) => {
                if val < 1 {
                    steps_per_second.set_text("1");
                    1
                } else if val > MAX_STEPS_PER_SECOND {
                    steps_per_second.set_text(&MAX_STEPS_PER_SECOND.to_string());
                    MAX_STEPS_PER_SECOND
                } else {
                    val
                }
            },
            Err(_) => {
                //if we somehow get a non-integer value or we're out of range, just reset it to 10.
                steps_per_second.set_text("10");
                10
            }
        }
    };
    let mut cycles = 0;
    let mut steps_completed_in_cycle = 0;
    glib::timeout_add_local(1, move || {
        let steps_per_second = get_steps_per_second();
        let steps_per_millis = steps_per_second / 1000;
        let mut on_cycle = 1;
        //if we need to do less than 1000 steps/sec, so we need to determine if this is a cycle we should do a step
        if steps_per_millis == 0 {
            //how many steps should be completed by the time we complete this cycle?
            let should_complete = cycles * steps_per_second / 1000;
            //println!("should complete = {}", should_complete);
            on_cycle = if steps_completed_in_cycle <= should_complete {1} else {0};
        }
        let mutex_paused: &Mutex<bool> = paused_1.borrow();
        match mutex_paused.lock() {
            Ok(mut is_paused) => {
                //println!("on_cycle = {}, should_complete = {}, completed = {}", on_cycle, should_complete, steps_completed_in_cycle);
                if !(*is_paused) {
                    cycles = (cycles + 1) % 1000;
                    if cycles == 0 {
                        steps_completed_in_cycle = 0;
                    }
                    if on_cycle != 0 {
                        let repeats = max(steps_per_millis, 1);
                        for i in 0..repeats {
                            let should_continue = perform_step_1();
                            if !should_continue {
                                *is_paused = true;
                            }
                            steps_completed_in_cycle += 1;
                        }
                    }
                }
                glib::source::Continue(true)
            }
            Err(_) => {
                panic!("Error acquiring lock on pause state.")
            },
        }
    });

    let play_button: gtk::Button = builder.get_object("play").unwrap();
    play_button.connect_clicked(move |but| {
        let mutex_paused: &Mutex<bool> = paused.borrow();
        match mutex_paused.lock() {
            Ok(mut guard_paused) => {
                *guard_paused = false;
            },
            Err(_) => {
                panic!("Error acquiring lock on pause state.")
            },
        }
    });

    // [ |< First ] button -- reset simulation to first time-step.
    let play_button: gtk::Button = builder.get_object("play").unwrap();
    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    let first_button: gtk::Button = builder.get_object("first").unwrap();
    let next_button: gtk::Button = builder.get_object("next").unwrap();
    let file_chooser: gtk::FileChooserButton = builder.get_object("open").unwrap();
    let displays: [gtk::Box; 13] = [
        builder.get_object("l1ic_display").unwrap(),
        builder.get_object("l1dc_display").unwrap(),
        builder.get_object("l2c_display").unwrap(),
        builder.get_object("l3c_display").unwrap(),
        builder.get_object("l1ip_display").unwrap(),
        builder.get_object("l1dp_display").unwrap(),
        builder.get_object("l2p_display").unwrap(),
        builder.get_object("l3p_display").unwrap(),
        builder.get_object("bp1_display").unwrap(),
        builder.get_object("bp2_display").unwrap(),
        builder.get_object("bp3_display").unwrap(),
        builder.get_object("bp4_display").unwrap(),
        builder.get_object("char_display").unwrap()
    ];
    let l1ic_accesses: gtk::Label = builder.get_object("l1ic_accesses").unwrap();
    let l1ic_hits: gtk::Label = builder.get_object("l1ic_hits").unwrap();
    let l1ic_rate: gtk::Label = builder.get_object("l1ic_rate").unwrap();
    let l1dc_accesses: gtk::Label = builder.get_object("l1dc_accesses").unwrap();
    let l1dc_hits: gtk::Label = builder.get_object("l1dc_hits").unwrap();
    let l1dc_rate: gtk::Label = builder.get_object("l1dc_rate").unwrap();
    let l2c_accesses: gtk::Label = builder.get_object("l2c_accesses").unwrap();
    let l2c_hits: gtk::Label = builder.get_object("l2c_hits").unwrap();
    let l2c_rate: gtk::Label = builder.get_object("l2c_rate").unwrap();
    let l3c_accesses: gtk::Label = builder.get_object("l3c_accesses").unwrap();
    let l3c_hits: gtk::Label = builder.get_object("l3c_hits").unwrap();
    let l3c_rate: gtk::Label = builder.get_object("l3c_rate").unwrap();
    let l1ip_accesses: gtk::Label = builder.get_object("l1ip_accesses").unwrap();
    let l1ip_hits: gtk::Label = builder.get_object("l1ip_hits").unwrap();
    let l1ip_rate: gtk::Label = builder.get_object("l1ip_rate").unwrap();
    let l1ip_issued: gtk::Label = builder.get_object("l1ip_issued").unwrap();
    let l1ip_extraneous: gtk::Label = builder.get_object("l1ip_extraneous").unwrap();
    let l1ip_accuracy: gtk::Label = builder.get_object("l1ip_accuracy").unwrap();
    let l1dp_accesses: gtk::Label = builder.get_object("l1dp_accesses").unwrap();
    let l1dp_hits: gtk::Label = builder.get_object("l1dp_hits").unwrap();
    let l1dp_rate: gtk::Label = builder.get_object("l1dp_rate").unwrap();
    let l1dp_issued: gtk::Label = builder.get_object("l1dp_issued").unwrap();
    let l1dp_extraneous: gtk::Label = builder.get_object("l1dp_extraneous").unwrap();
    let l1dp_accuracy: gtk::Label = builder.get_object("l1dp_accuracy").unwrap();
    let l2p_accesses: gtk::Label = builder.get_object("l2p_accesses").unwrap();
    let l2p_hits: gtk::Label = builder.get_object("l2p_hits").unwrap();
    let l2p_rate: gtk::Label = builder.get_object("l2p_rate").unwrap();
    let l2p_issued: gtk::Label = builder.get_object("l2p_issued").unwrap();
    let l2p_extraneous: gtk::Label = builder.get_object("l2p_extraneous").unwrap();
    let l2p_accuracy: gtk::Label = builder.get_object("l2p_accuracy").unwrap();
    let l3p_accesses: gtk::Label = builder.get_object("l3p_accesses").unwrap();
    let l3p_hits: gtk::Label = builder.get_object("l3p_hits").unwrap();
    let l3p_rate: gtk::Label = builder.get_object("l3p_rate").unwrap();
    let l3p_issued: gtk::Label = builder.get_object("l3p_issued").unwrap();
    let l3p_extraneous: gtk::Label = builder.get_object("l3p_extraneous").unwrap();
    let l3p_accuracy: gtk::Label = builder.get_object("l3p_accuracy").unwrap();
    let bp1_accesses: gtk::Label = builder.get_object("bp1_accesses").unwrap();
    let bp1_hits: gtk::Label = builder.get_object("bp1_hits").unwrap();
    let bp1_rate: gtk::Label = builder.get_object("bp1_rate").unwrap();
    let bp2_accesses: gtk::Label = builder.get_object("bp2_accesses").unwrap();
    let bp2_hits: gtk::Label = builder.get_object("bp2_hits").unwrap();
    let bp2_rate: gtk::Label = builder.get_object("bp2_rate").unwrap();
    let bp3_accesses: gtk::Label = builder.get_object("bp3_accesses").unwrap();
    let bp3_hits: gtk::Label = builder.get_object("bp3_hits").unwrap();
    let bp3_rate: gtk::Label = builder.get_object("bp3_rate").unwrap();
    let bp4_accesses: gtk::Label = builder.get_object("bp4_accesses").unwrap();
    let bp4_hits: gtk::Label = builder.get_object("bp4_hits").unwrap();
    let bp4_rate: gtk::Label = builder.get_object("bp4_rate").unwrap();

    first_button.connect_clicked(move |_| {
        pause_button.set_visible(false);
        play_button.set_visible(true);
        let mtx_paused: &Mutex<bool> = paused_3.borrow();
        match mtx_paused.lock() {
            Ok(mut is_paused) => {
                *is_paused = true;
            },
            Err(_) => {
                panic!("Could not acquire lock for pause state when resetting simulation");
            },
        }
        //clear all display rows
        for display in displays.iter() {
            for child in display.get_children() {
                unsafe { child.destroy() } ;
            }
        }

        //reset left pane stats
        l1ic_accesses.set_text("0");
        l1ic_hits.set_text("0");
        l1ic_rate.set_text("0.00%");
        l1dc_accesses.set_text("0");
        l1dc_hits.set_text("0");
        l1dc_rate.set_text("0.00%");
        l2c_accesses.set_text("0");
        l2c_hits.set_text("0");
        l2c_rate.set_text("0.00%");
        l3c_accesses.set_text("0");
        l3c_hits.set_text("0");
        l3c_rate.set_text("0.00%");
        l1ip_accesses.set_text("0");
        l1ip_hits.set_text("0");
        l1ip_rate.set_text("0.00%");
        l1ip_issued.set_text("0");
        l1ip_extraneous.set_text("0");
        l1ip_accuracy.set_text("0.00%");
        l1dp_accesses.set_text("0");
        l1dp_hits.set_text("0");
        l1dp_rate.set_text("0.00%");
        l1dp_issued.set_text("0");
        l1dp_extraneous.set_text("0");
        l1dp_accuracy.set_text("0.00%");
        l2p_accesses.set_text("0");
        l2p_hits.set_text("0");
        l2p_rate.set_text("0.00%");
        l2p_issued.set_text("0");
        l2p_extraneous.set_text("0");
        l2p_accuracy.set_text("0.00%");
        l3p_accesses.set_text("0");
        l3p_hits.set_text("0");
        l3p_rate.set_text("0.00%");
        l3p_issued.set_text("0");
        l3p_extraneous.set_text("0");
        l3p_accuracy.set_text("0.00%");
        bp1_accesses.set_text("0");
        bp1_hits.set_text("0");
        bp1_rate.set_text("0.00%");
        bp2_accesses.set_text("0");
        bp2_hits.set_text("0");
        bp2_rate.set_text("0.00%");
        bp3_accesses.set_text("0");
        bp3_hits.set_text("0");
        bp3_rate.set_text("0.00%");
        bp4_accesses.set_text("0");
        bp4_hits.set_text("0");
        bp4_rate.set_text("0.00%");


        match dumpfile_2.lock() {
            Ok(mut dumpfile) => {
                match file_chooser.get_filename() {
                    Some(filename) => {
                        *dumpfile = Option::from(Dumpfile::new(&filename).unwrap());
                        match predictor_stats_2.lock() {
                            Ok(mut stats_array) => {
                                for i in 0..9 {
                                    stats_array.0[i] = Option::from(OverallStats::new(dumpfile.as_ref().unwrap().line_size, dumpfile.as_ref().unwrap().page_size));
                                }
                                stats_array.1 = Option::from(ACBiasStats::new());
                            },
                            Err(_) => {
                                panic!("Could not acquire lock to initialize predictor stats")
                            },
                        }
                    }
                    None => {
                        panic!("Should never get here");
                    }
                }
            }
            Err(_) => {
                panic!("Could not acquire lock to reset dumpfile progress on simulation restart");
            }
        }

        next_button.set_sensitive(true);
        play_button.set_sensitive(true);
        pause_button.set_sensitive(true);
    });


    let play_button: gtk::Button = builder.get_object("play").unwrap();
    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    play_button.connect_clicked( move |but| {
        but.set_visible(false);
        pause_button.set_visible(true);
    });

    let play_button: gtk::Button = builder.get_object("play").unwrap();
    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    pause_button.connect_clicked(move |but| {
        but.set_visible(false);
        play_button.set_visible(true);
        match paused_4.lock() {
            Ok(mut is_paused) => {
                *is_paused = true;
            }
            Err(_) => {
                panic!("Could not acquire lock for pause state when pause button was clicked.");
            }
        }
    });

    let l1ic_min_addr: gtk::Entry = builder.get_object("l1ic_min_addr").unwrap();
    let l1ic_max_addr: gtk::Entry = builder.get_object("l1ic_max_addr").unwrap();
    let l1dc_min_addr: gtk::Entry = builder.get_object("l1dc_min_addr").unwrap();
    let l1dc_max_addr: gtk::Entry = builder.get_object("l1dc_max_addr").unwrap();
    let l2c_min_addr: gtk::Entry = builder.get_object("l2c_min_addr").unwrap();
    let l2c_max_addr: gtk::Entry = builder.get_object("l2c_max_addr").unwrap();
    let l3c_min_addr: gtk::Entry = builder.get_object("l3c_min_addr").unwrap();
    let l3c_max_addr: gtk::Entry = builder.get_object("l3c_max_addr").unwrap();
    let l1ip_min_addr: gtk::Entry = builder.get_object("l1ip_min_addr").unwrap();
    let l1ip_max_addr: gtk::Entry = builder.get_object("l1ip_max_addr").unwrap();
    let l1dp_min_addr: gtk::Entry = builder.get_object("l1dp_min_addr").unwrap();
    let l1dp_max_addr: gtk::Entry = builder.get_object("l1dp_max_addr").unwrap();
    let l2p_min_addr: gtk::Entry = builder.get_object("l2p_min_addr").unwrap();
    let l2p_max_addr: gtk::Entry = builder.get_object("l2p_max_addr").unwrap();
    let l3p_min_addr: gtk::Entry = builder.get_object("l3p_min_addr").unwrap();
    let l3p_max_addr: gtk::Entry = builder.get_object("l3p_max_addr").unwrap();
    let bp1_min_addr: gtk::Entry = builder.get_object("bp1_min_addr").unwrap();
    let bp1_max_addr: gtk::Entry = builder.get_object("bp1_max_addr").unwrap();
    let bp2_min_addr: gtk::Entry = builder.get_object("bp2_min_addr").unwrap();
    let bp2_max_addr: gtk::Entry = builder.get_object("bp2_max_addr").unwrap();
    let bp3_min_addr: gtk::Entry = builder.get_object("bp3_min_addr").unwrap();
    let bp3_max_addr: gtk::Entry = builder.get_object("bp3_max_addr").unwrap();
    let bp4_min_addr: gtk::Entry = builder.get_object("bp4_min_addr").unwrap();
    let bp4_max_addr: gtk::Entry = builder.get_object("bp4_max_addr").unwrap();
    let char_min_addr: gtk::Entry = builder.get_object("char_min_addr").unwrap();
    let char_max_addr: gtk::Entry = builder.get_object("char_max_addr").unwrap();
    let l1ic_cpus: gtk::Box = builder.get_object("l1ic_cpus").unwrap();
    let l1dc_cpus: gtk::Box = builder.get_object("l1dc_cpus").unwrap();
    let l2c_cpus: gtk::Box = builder.get_object("l2c_cpus").unwrap();
    let l1ip_cpus: gtk::Box = builder.get_object("l1ip_cpus").unwrap();
    let l1dp_cpus: gtk::Box = builder.get_object("l1dp_cpus").unwrap();
    let l2p_cpus: gtk::Box = builder.get_object("l2p_cpus").unwrap();
    let bp1_cpus: gtk::Box = builder.get_object("bp1_cpus").unwrap();
    let bp2_cpus: gtk::Box = builder.get_object("bp2_cpus").unwrap();
    let bp3_cpus: gtk::Box = builder.get_object("bp3_cpus").unwrap();
    let bp4_cpus: gtk::Box = builder.get_object("bp4_cpus").unwrap();
    let char_cpus: gtk::Box = builder.get_object("char_cpus").unwrap();

    let l1ic_accesses: gtk::Label = builder.get_object("l1ic_accesses").unwrap();
    let l1ic_hits: gtk::Label = builder.get_object("l1ic_hits").unwrap();
    let l1ic_rate: gtk::Label = builder.get_object("l1ic_rate").unwrap();
    let l1dc_accesses: gtk::Label = builder.get_object("l1dc_accesses").unwrap();
    let l1dc_hits: gtk::Label = builder.get_object("l1dc_hits").unwrap();
    let l1dc_rate: gtk::Label = builder.get_object("l1dc_rate").unwrap();
    let l2c_accesses: gtk::Label = builder.get_object("l2c_accesses").unwrap();
    let l2c_hits: gtk::Label = builder.get_object("l2c_hits").unwrap();
    let l2c_rate: gtk::Label = builder.get_object("l2c_rate").unwrap();
    let l3c_accesses: gtk::Label = builder.get_object("l3c_accesses").unwrap();
    let l3c_hits: gtk::Label = builder.get_object("l3c_hits").unwrap();
    let l3c_rate: gtk::Label = builder.get_object("l3c_rate").unwrap();
    let l1ip_accesses: gtk::Label = builder.get_object("l1ip_accesses").unwrap();
    let l1ip_hits: gtk::Label = builder.get_object("l1ip_hits").unwrap();
    let l1ip_rate: gtk::Label = builder.get_object("l1ip_rate").unwrap();
    let l1ip_issued: gtk::Label = builder.get_object("l1ip_issued").unwrap();
    let l1ip_extraneous: gtk::Label = builder.get_object("l1ip_extraneous").unwrap();
    let l1ip_accuracy: gtk::Label = builder.get_object("l1ip_accuracy").unwrap();
    let l1dp_accesses: gtk::Label = builder.get_object("l1dp_accesses").unwrap();
    let l1dp_hits: gtk::Label = builder.get_object("l1dp_hits").unwrap();
    let l1dp_rate: gtk::Label = builder.get_object("l1dp_rate").unwrap();
    let l1dp_issued: gtk::Label = builder.get_object("l1dp_issued").unwrap();
    let l1dp_extraneous: gtk::Label = builder.get_object("l1dp_extraneous").unwrap();
    let l1dp_accuracy: gtk::Label = builder.get_object("l1dp_accuracy").unwrap();
    let l2p_accesses: gtk::Label = builder.get_object("l2p_accesses").unwrap();
    let l2p_hits: gtk::Label = builder.get_object("l2p_hits").unwrap();
    let l2p_rate: gtk::Label = builder.get_object("l2p_rate").unwrap();
    let l2p_issued: gtk::Label = builder.get_object("l2p_issued").unwrap();
    let l2p_extraneous: gtk::Label = builder.get_object("l2p_extraneous").unwrap();
    let l2p_accuracy: gtk::Label = builder.get_object("l2p_accuracy").unwrap();
    let l3p_accesses: gtk::Label = builder.get_object("l3p_accesses").unwrap();
    let l3p_hits: gtk::Label = builder.get_object("l3p_hits").unwrap();
    let l3p_rate: gtk::Label = builder.get_object("l3p_rate").unwrap();
    let l3p_issued: gtk::Label = builder.get_object("l3p_issued").unwrap();
    let l3p_extraneous: gtk::Label = builder.get_object("l3p_extraneous").unwrap();
    let l3p_accuracy: gtk::Label = builder.get_object("l3p_accuracy").unwrap();
    let bp1_accesses: gtk::Label = builder.get_object("bp1_accesses").unwrap();
    let bp1_hits: gtk::Label = builder.get_object("bp1_hits").unwrap();
    let bp1_rate: gtk::Label = builder.get_object("bp1_rate").unwrap();
    let bp2_accesses: gtk::Label = builder.get_object("bp2_accesses").unwrap();
    let bp2_hits: gtk::Label = builder.get_object("bp2_hits").unwrap();
    let bp2_rate: gtk::Label = builder.get_object("bp2_rate").unwrap();
    let bp3_accesses: gtk::Label = builder.get_object("bp3_accesses").unwrap();
    let bp3_hits: gtk::Label = builder.get_object("bp3_hits").unwrap();
    let bp3_rate: gtk::Label = builder.get_object("bp3_rate").unwrap();
    let bp4_accesses: gtk::Label = builder.get_object("bp4_accesses").unwrap();
    let bp4_hits: gtk::Label = builder.get_object("bp4_hits").unwrap();
    let bp4_rate: gtk::Label = builder.get_object("bp4_rate").unwrap();

    let char_registers: gtk::Box = builder.get_object("char_regs").unwrap();

    // (min addr, max addr, cpus, accesses, hits, rate/coverage, issued, extraneous, accuracy)
    let l1ic_controls = (l1ic_min_addr, l1ic_max_addr, Option::from(l1ic_cpus), l1ic_accesses, l1ic_hits, l1ic_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1dc_controls = (l1dc_min_addr, l1dc_max_addr, Option::from(l1dc_cpus), l1dc_accesses, l1dc_hits, l1dc_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l2c_controls = (l2c_min_addr, l2c_max_addr, Option::from(l2c_cpus), l2c_accesses, l2c_hits, l2c_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l3c_controls = (l3c_min_addr, l3c_max_addr, Option::None::<gtk::Box>, l3c_accesses, l3c_hits, l3c_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1ip_controls = (l1ip_min_addr, l1ip_max_addr, Option::from(l1ip_cpus), l1ip_accesses, l1ip_hits, l1ip_rate, Option::from(l1ip_issued), Option::from(l1ip_extraneous), Option::from(l1ip_accuracy));
    let l1dp_controls = (l1dp_min_addr, l1dp_max_addr, Option::from(l1dp_cpus), l1dp_accesses, l1dp_hits, l1dp_rate, Option::from(l1dp_issued), Option::from(l1dp_extraneous), Option::from(l1dp_accuracy));
    let l2p_controls = (l2p_min_addr, l2p_max_addr, Option::from(l2p_cpus), l2p_accesses, l2p_hits, l2p_rate, Option::from(l2p_issued), Option::from(l2p_extraneous), Option::from(l2p_accuracy));
    let l3p_controls = (l3p_min_addr, l3p_max_addr, Option::None::<gtk::Box>, l3p_accesses, l3p_hits, l3p_rate, Option::from(l3p_issued), Option::from(l3p_extraneous), Option::from(l3p_accuracy));
    let bp1_controls = (bp1_min_addr, bp1_max_addr, Option::from(bp1_cpus), bp1_accesses, bp1_hits, bp1_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp2_controls = (bp2_min_addr, bp2_max_addr, Option::from(bp2_cpus), bp2_accesses, bp2_hits, bp2_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp3_controls = (bp3_min_addr, bp3_max_addr, Option::from(bp3_cpus), bp3_accesses, bp3_hits, bp3_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp4_controls = (bp4_min_addr, bp4_max_addr, Option::from(bp4_cpus), bp4_accesses, bp4_hits, bp4_rate, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);

    let char_controls = (char_min_addr, char_max_addr, char_cpus, char_registers);

    //another copy of everything because rust won't let us read and modify the same object at the same time.
    let l1ic_min_addr_2: gtk::Entry = builder.get_object("l1ic_min_addr").unwrap();
    let l1ic_max_addr_2: gtk::Entry = builder.get_object("l1ic_max_addr").unwrap();
    let l1dc_min_addr_2: gtk::Entry = builder.get_object("l1dc_min_addr").unwrap();
    let l1dc_max_addr_2: gtk::Entry = builder.get_object("l1dc_max_addr").unwrap();
    let l2c_min_addr_2: gtk::Entry = builder.get_object("l2c_min_addr").unwrap();
    let l2c_max_addr_2: gtk::Entry = builder.get_object("l2c_max_addr").unwrap();
    let l3c_min_addr_2: gtk::Entry = builder.get_object("l3c_min_addr").unwrap();
    let l3c_max_addr_2: gtk::Entry = builder.get_object("l3c_max_addr").unwrap();
    let l1ip_min_addr_2: gtk::Entry = builder.get_object("l1ip_min_addr").unwrap();
    let l1ip_max_addr_2: gtk::Entry = builder.get_object("l1ip_max_addr").unwrap();
    let l1dp_min_addr_2: gtk::Entry = builder.get_object("l1dp_min_addr").unwrap();
    let l1dp_max_addr_2: gtk::Entry = builder.get_object("l1dp_max_addr").unwrap();
    let l2p_min_addr_2: gtk::Entry = builder.get_object("l2p_min_addr").unwrap();
    let l2p_max_addr_2: gtk::Entry = builder.get_object("l2p_max_addr").unwrap();
    let l3p_min_addr_2: gtk::Entry = builder.get_object("l3p_min_addr").unwrap();
    let l3p_max_addr_2: gtk::Entry = builder.get_object("l3p_max_addr").unwrap();
    let bp1_min_addr_2: gtk::Entry = builder.get_object("bp1_min_addr").unwrap();
    let bp1_max_addr_2: gtk::Entry = builder.get_object("bp1_max_addr").unwrap();
    let bp2_min_addr_2: gtk::Entry = builder.get_object("bp2_min_addr").unwrap();
    let bp2_max_addr_2: gtk::Entry = builder.get_object("bp2_max_addr").unwrap();
    let bp3_min_addr_2: gtk::Entry = builder.get_object("bp3_min_addr").unwrap();
    let bp3_max_addr_2: gtk::Entry = builder.get_object("bp3_max_addr").unwrap();
    let bp4_min_addr_2: gtk::Entry = builder.get_object("bp4_min_addr").unwrap();
    let bp4_max_addr_2: gtk::Entry = builder.get_object("bp4_max_addr").unwrap();
    let char_min_addr_2: gtk::Entry = builder.get_object("char_min_addr").unwrap();
    let char_max_addr_2: gtk::Entry = builder.get_object("char_max_addr").unwrap();
    let l1ic_cpus_2: gtk::Box = builder.get_object("l1ic_cpus").unwrap();
    let l1dc_cpus_2: gtk::Box = builder.get_object("l1dc_cpus").unwrap();
    let l2c_cpus_2: gtk::Box = builder.get_object("l2c_cpus").unwrap();
    let l1ip_cpus_2: gtk::Box = builder.get_object("l1ip_cpus").unwrap();
    let l1dp_cpus_2: gtk::Box = builder.get_object("l1dp_cpus").unwrap();
    let l2p_cpus_2: gtk::Box = builder.get_object("l2p_cpus").unwrap();
    let bp1_cpus_2: gtk::Box = builder.get_object("bp1_cpus").unwrap();
    let bp2_cpus_2: gtk::Box = builder.get_object("bp2_cpus").unwrap();
    let bp3_cpus_2: gtk::Box = builder.get_object("bp3_cpus").unwrap();
    let bp4_cpus_2: gtk::Box = builder.get_object("bp4_cpus").unwrap();
    let char_cpus_2: gtk::Box = builder.get_object("char_cpus").unwrap();

    let l1ic_accesses_2: gtk::Label = builder.get_object("l1ic_accesses").unwrap();
    let l1ic_hits_2: gtk::Label = builder.get_object("l1ic_hits").unwrap();
    let l1ic_rate_2: gtk::Label = builder.get_object("l1ic_rate").unwrap();
    let l1dc_accesses_2: gtk::Label = builder.get_object("l1dc_accesses").unwrap();
    let l1dc_hits_2: gtk::Label = builder.get_object("l1dc_hits").unwrap();
    let l1dc_rate_2: gtk::Label = builder.get_object("l1dc_rate").unwrap();
    let l2c_accesses_2: gtk::Label = builder.get_object("l2c_accesses").unwrap();
    let l2c_hits_2: gtk::Label = builder.get_object("l2c_hits").unwrap();
    let l2c_rate_2: gtk::Label = builder.get_object("l2c_rate").unwrap();
    let l3c_accesses_2: gtk::Label = builder.get_object("l3c_accesses").unwrap();
    let l3c_hits_2: gtk::Label = builder.get_object("l3c_hits").unwrap();
    let l3c_rate_2: gtk::Label = builder.get_object("l3c_rate").unwrap();
    let l1ip_accesses_2: gtk::Label = builder.get_object("l1ip_accesses").unwrap();
    let l1ip_hits_2: gtk::Label = builder.get_object("l1ip_hits").unwrap();
    let l1ip_rate_2: gtk::Label = builder.get_object("l1ip_rate").unwrap();
    let l1ip_issued_2: gtk::Label = builder.get_object("l1ip_issued").unwrap();
    let l1ip_extraneous_2: gtk::Label = builder.get_object("l1ip_extraneous").unwrap();
    let l1ip_accuracy_2: gtk::Label = builder.get_object("l1ip_accuracy").unwrap();
    let l1dp_accesses_2: gtk::Label = builder.get_object("l1dp_accesses").unwrap();
    let l1dp_hits_2: gtk::Label = builder.get_object("l1dp_hits").unwrap();
    let l1dp_rate_2: gtk::Label = builder.get_object("l1dp_rate").unwrap();
    let l1dp_issued_2: gtk::Label = builder.get_object("l1dp_issued").unwrap();
    let l1dp_extraneous_2: gtk::Label = builder.get_object("l1dp_extraneous").unwrap();
    let l1dp_accuracy_2: gtk::Label = builder.get_object("l1dp_accuracy").unwrap();
    let l2p_accesses_2: gtk::Label = builder.get_object("l2p_accesses").unwrap();
    let l2p_hits_2: gtk::Label = builder.get_object("l2p_hits").unwrap();
    let l2p_rate_2: gtk::Label = builder.get_object("l2p_rate").unwrap();
    let l2p_issued_2: gtk::Label = builder.get_object("l2p_issued").unwrap();
    let l2p_extraneous_2: gtk::Label = builder.get_object("l2p_extraneous").unwrap();
    let l2p_accuracy_2: gtk::Label = builder.get_object("l2p_accuracy").unwrap();
    let l3p_accesses_2: gtk::Label = builder.get_object("l3p_accesses").unwrap();
    let l3p_hits_2: gtk::Label = builder.get_object("l3p_hits").unwrap();
    let l3p_rate_2: gtk::Label = builder.get_object("l3p_rate").unwrap();
    let l3p_issued_2: gtk::Label = builder.get_object("l3p_issued").unwrap();
    let l3p_extraneous_2: gtk::Label = builder.get_object("l3p_extraneous").unwrap();
    let l3p_accuracy_2: gtk::Label = builder.get_object("l3p_accuracy").unwrap();
    let bp1_accesses_2: gtk::Label = builder.get_object("bp1_accesses").unwrap();
    let bp1_hits_2: gtk::Label = builder.get_object("bp1_hits").unwrap();
    let bp1_rate_2: gtk::Label = builder.get_object("bp1_rate").unwrap();
    let bp2_accesses_2: gtk::Label = builder.get_object("bp2_accesses").unwrap();
    let bp2_hits_2: gtk::Label = builder.get_object("bp2_hits").unwrap();
    let bp2_rate_2: gtk::Label = builder.get_object("bp2_rate").unwrap();
    let bp3_accesses_2: gtk::Label = builder.get_object("bp3_accesses").unwrap();
    let bp3_hits_2: gtk::Label = builder.get_object("bp3_hits").unwrap();
    let bp3_rate_2: gtk::Label = builder.get_object("bp3_rate").unwrap();
    let bp4_accesses_2: gtk::Label = builder.get_object("bp4_accesses").unwrap();
    let bp4_hits_2: gtk::Label = builder.get_object("bp4_hits").unwrap();
    let bp4_rate_2: gtk::Label = builder.get_object("bp4_rate").unwrap();

    let char_registers_2: gtk::Box = builder.get_object("char_regs").unwrap();

    // (min addr, max addr, cpus, accesses, hits, rate/coverage, issued, extraneous, accuracy)
    let l1ic_controls_2 = (l1ic_min_addr_2, l1ic_max_addr_2, Option::from(l1ic_cpus_2), l1ic_accesses_2, l1ic_hits_2, l1ic_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1dc_controls_2 = (l1dc_min_addr_2, l1dc_max_addr_2, Option::from(l1dc_cpus_2), l1dc_accesses_2, l1dc_hits_2, l1dc_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l2c_controls_2 = (l2c_min_addr_2, l2c_max_addr_2, Option::from(l2c_cpus_2), l2c_accesses_2, l2c_hits_2, l2c_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l3c_controls_2 = (l3c_min_addr_2, l3c_max_addr_2, Option::None::<gtk::Box>, l3c_accesses_2, l3c_hits_2, l3c_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let l1ip_controls_2 = (l1ip_min_addr_2, l1ip_max_addr_2, Option::from(l1ip_cpus_2), l1ip_accesses_2, l1ip_hits_2, l1ip_rate_2, Option::from(l1ip_issued_2), Option::from(l1ip_extraneous_2), Option::from(l1ip_accuracy_2));
    let l1dp_controls_2 = (l1dp_min_addr_2, l1dp_max_addr_2, Option::from(l1dp_cpus_2), l1dp_accesses_2, l1dp_hits_2, l1dp_rate_2, Option::from(l1dp_issued_2), Option::from(l1dp_extraneous_2), Option::from(l1dp_accuracy_2));
    let l2p_controls_2 = (l2p_min_addr_2, l2p_max_addr_2, Option::from(l2p_cpus_2), l2p_accesses_2, l2p_hits_2, l2p_rate_2, Option::from(l2p_issued_2), Option::from(l2p_extraneous_2), Option::from(l2p_accuracy_2));
    let l3p_controls_2 = (l3p_min_addr_2, l3p_max_addr_2, Option::None::<gtk::Box>, l3p_accesses_2, l3p_hits_2, l3p_rate_2, Option::from(l3p_issued_2), Option::from(l3p_extraneous_2), Option::from(l3p_accuracy_2));
    let bp1_controls_2 = (bp1_min_addr_2, bp1_max_addr_2, Option::from(bp1_cpus_2), bp1_accesses_2, bp1_hits_2, bp1_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp2_controls_2 = (bp2_min_addr_2, bp2_max_addr_2, Option::from(bp2_cpus_2), bp2_accesses_2, bp2_hits_2, bp2_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp3_controls_2 = (bp3_min_addr_2, bp3_max_addr_2, Option::from(bp3_cpus_2), bp3_accesses_2, bp3_hits_2, bp3_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);
    let bp4_controls_2 = (bp4_min_addr_2, bp4_max_addr_2, Option::from(bp4_cpus_2), bp4_accesses_2, bp4_hits_2, bp4_rate_2, Option::None::<gtk::Label>, Option::None::<gtk::Label>, Option::None::<gtk::Label>);

    let char_controls_2 = (char_min_addr_2, char_max_addr_2, char_cpus_2, char_registers_2);


    //this function is for everything except the characterization tab (register biases)
    let update_stats_wrapper = |controls: (gtk::Entry, gtk::Entry, Option<gtk::Box>, gtk::Label, gtk::Label, gtk::Label, Option<gtk::Label>, Option<gtk::Label>, Option<gtk::Label>), controls_2, i, stats: Arc<Mutex<([Option<OverallStats>; 9], Option<ACBiasStats>)>>| {
        //we annoyingly need a separate copy for each control element
        let stats_1 = stats.clone();
        let stats_2 = stats.clone();
        let stats_3 = stats.clone();
        let stats_4 = stats.clone();
        let stats_5 = stats.clone();
        let stats_6 = stats.clone();
        let stats_7 = stats.clone();
        let stats_8 = stats.clone();
        let stats_9 = stats.clone();
        let stats_10 = stats.clone();
        let stats_11 = stats.clone();
        let stats_12 = stats.clone();
        let stats_13 = stats.clone();
        let stats_14 = stats.clone();
        let stats_15 = stats.clone();
        let stats_16 = stats.clone();
        let stats_17 = stats.clone();

        let arc_controls_2 = Arc::new(Mutex::new(controls_2));
        let arc_controls_2_1 = arc_controls_2.clone();
        let arc_controls_2_2 = arc_controls_2.clone();
        let arc_controls_2_3 = arc_controls_2.clone();
        let arc_controls_2_4 = arc_controls_2.clone();
        let arc_controls_2_5 = arc_controls_2.clone();
        let arc_controls_2_6 = arc_controls_2.clone();
        let arc_controls_2_7 = arc_controls_2.clone();
        let arc_controls_2_8 = arc_controls_2.clone();
        let arc_controls_2_9 = arc_controls_2.clone();
        let arc_controls_2_10 = arc_controls_2.clone();
        let arc_controls_2_11 = arc_controls_2.clone();
        let arc_controls_2_12 = arc_controls_2.clone();
        let arc_controls_2_13 = arc_controls_2.clone();
        let arc_controls_2_14 = arc_controls_2.clone();
        let arc_controls_2_15 = arc_controls_2.clone();
        let arc_controls_2_16 = arc_controls_2.clone();
        let arc_controls_2_17 = arc_controls_2.clone();
        controls.0.connect_changed(move |text| {
            let stats = stats.lock().expect("Could not acquire lock for predictor stats when updating for filter change.");
            let arr: &[Option<OverallStats>; 9] = &stats.0;
            match &arr[i] {
                Some(single_predictor_stats) => {
                    let controls_2 = &*(arc_controls_2.lock().expect("Could not acquire lock for UI stat display when updating for filter change"));
                    update_stats_for_filter(&single_predictor_stats, controls_2);
                }
                None => {}
            }
        });
        controls.1.connect_changed(move |text| {
            let stats = stats_1.lock().expect("Could not acquire lock for predictor stats when updating for filter change.");
            let arr: &[Option<OverallStats>; 9] = &stats.0;
            match &arr[i] {
                Some(single_predictor_stats) => {
                    let controls_2 = &*(arc_controls_2_1.lock().expect("Could not acquire lock for UI stat display when updating for filter change"));
                    update_stats_for_filter(&single_predictor_stats, controls_2);
                }
                None => {}
            }
        });
        match controls.2 {
            Some(visual_cpu_box) => {
                let rows = visual_cpu_box.get_children();
                let row_1 = rows.get(0).unwrap().downcast_ref::<gtk::Box>().unwrap();
                let row_2 = rows.get(1).unwrap().downcast_ref::<gtk::Box>().unwrap();
                let row_1_check_boxes = row_1.get_children();
                let row_2_check_boxes = row_2.get_children();
                //signature of this closure is a mess, but it avoids having to write it separately for all 16 cpu checkboxes
                let checkbox_update_wrapper = |checkbox: &gtk::Widget, stats: Arc<Mutex<([Option<OverallStats>; 9], Option<ACBiasStats>)>>, i: usize,
                                               arc_controls_2: Arc<Mutex<(gtk::Entry, gtk::Entry, Option<gtk::Box>, gtk::Label, gtk::Label, gtk::Label, Option<gtk::Label>, Option<gtk::Label>, Option<gtk::Label>)>>| {
                    checkbox.downcast_ref::<gtk::CheckButton>().unwrap().connect_toggled(move |_| {
                        let mtx_stats = stats.lock().expect("Could not acquire lock for predictor stats when updating for filter change.");
                        let arr: &[Option<OverallStats>; 9] = &mtx_stats.0;
                        match &arr[i] {
                            Some(single_predictor_stats) => {
                                let controls_2 = &*(arc_controls_2.lock().expect("Could not acquire lock for UI stat display when updating for filter change"));
                                update_stats_for_filter(&single_predictor_stats, &controls_2);
                            }
                            None => {}
                        }
                    });
                };

                checkbox_update_wrapper(&row_1_check_boxes[0], stats_2, i, arc_controls_2_2);
                checkbox_update_wrapper(&row_1_check_boxes[1], stats_3, i, arc_controls_2_3);
                checkbox_update_wrapper(&row_1_check_boxes[2], stats_4, i, arc_controls_2_4);
                checkbox_update_wrapper(&row_1_check_boxes[3], stats_5, i, arc_controls_2_5);
                checkbox_update_wrapper(&row_1_check_boxes[4], stats_6, i, arc_controls_2_6);
                checkbox_update_wrapper(&row_1_check_boxes[5], stats_7, i, arc_controls_2_7);
                checkbox_update_wrapper(&row_1_check_boxes[6], stats_8, i, arc_controls_2_8);
                checkbox_update_wrapper(&row_1_check_boxes[7], stats_9, i, arc_controls_2_9);
                checkbox_update_wrapper(&row_2_check_boxes[0], stats_10, i, arc_controls_2_10);
                checkbox_update_wrapper(&row_2_check_boxes[1], stats_11, i, arc_controls_2_11);
                checkbox_update_wrapper(&row_2_check_boxes[2], stats_12, i, arc_controls_2_12);
                checkbox_update_wrapper(&row_2_check_boxes[3], stats_13, i, arc_controls_2_13);
                checkbox_update_wrapper(&row_2_check_boxes[4], stats_14, i, arc_controls_2_14);
                checkbox_update_wrapper(&row_2_check_boxes[5], stats_15, i, arc_controls_2_15);
                checkbox_update_wrapper(&row_2_check_boxes[6], stats_16, i, arc_controls_2_16);
                checkbox_update_wrapper(&row_2_check_boxes[7], stats_17, i, arc_controls_2_17);
            }
            None => {}
        }
    };

    update_stats_wrapper(l1ic_controls, l1ic_controls_2, 0, predictor_stats_3);
    update_stats_wrapper(l1dc_controls, l1dc_controls_2, 1, predictor_stats_4);
    update_stats_wrapper(l2c_controls, l2c_controls_2, 2, predictor_stats_5);
    update_stats_wrapper(l3c_controls, l3c_controls_2, 3, predictor_stats_6);
    update_stats_wrapper(l1ip_controls, l1ip_controls_2, 4, predictor_stats_7);
    update_stats_wrapper(l1dp_controls, l1dp_controls_2, 5, predictor_stats_8);
    update_stats_wrapper(l2p_controls, l2p_controls_2, 6, predictor_stats_9);
    update_stats_wrapper(l3p_controls, l3p_controls_2, 7, predictor_stats_10);
    update_stats_wrapper(bp1_controls, bp1_controls_2, 8, predictor_stats_11);
    update_stats_wrapper(bp2_controls, bp2_controls_2, 9, predictor_stats_12);
    update_stats_wrapper(bp3_controls, bp3_controls_2, 10, predictor_stats_13);
    update_stats_wrapper(bp4_controls, bp4_controls_2, 11, predictor_stats_14);

    window.maximize();
    window.show_all();

    let pause_button: gtk::Button = builder.get_object("pause").unwrap();
    pause_button.set_visible(false);




    gtk::main();
}

fn main() {
    // Create a new application
    let app = gtk::Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default())
        .expect("Initialization failed...");
    app.connect_activate(|app| on_activate(app));
    // Run the application
    app.run(&std::env::args().collect::<Vec<_>>());
}