use crate::predictors::{Predictor, NUM_CPUS};
use std::collections::HashMap;
use std::collections::BTreeMap;
use std::ops::Range;
use std::collections::hash_map::{Entry};
use crate::registers::NUM_REGISTERS;

#[derive(Clone)]
#[derive(Copy)]
pub struct LineCpuStats {
    hits: u64,
    possible: u64,
    extraneous: u64,
    issued: u64,
    last_10: [(Outcome, u64); 10]
}

impl LineCpuStats {

    pub fn new() -> LineCpuStats {
        LineCpuStats {
            hits: 0,
            possible: 0,
            extraneous: 0,
            issued: 0,
            last_10: [(Outcome::Miss, 0x0); 10]
        }
    }

    pub fn put(&mut self, address: u64, hit: Outcome) {
        match hit {
            Outcome::Hit => {
                self.hits += 1;
                self.possible += 1;
            }
            Outcome::Miss => {
                self.possible += 1;
            }
            Outcome::Extraneous => {
                self.extraneous += 1;
            }
            Outcome::Issued => {
                self.issued += 1;
            }
        };
        for i in 0..self.last_10.len() - 1 {
            self.last_10[i] = self.last_10[i + 1];
        }
        self.last_10[self.last_10.len() - 1] = (hit, address);
    }
}

type LineMap = [Option<LineCpuStats>; NUM_CPUS];
type PageMap = BTreeMap<u64, LineMap>;

pub struct OverallStats {
    map: HashMap<u64, PageMap>,
    line_size: u64,
    page_size: u64
}

impl OverallStats {

    pub fn new(line_size: u64, page_size: u64) -> OverallStats {
        OverallStats {
            map: HashMap::new(),
            line_size,
            page_size
        }
    }

    pub fn put(&mut self, address: u64, cpu: usize, hit: Outcome) -> Result<(), u8> {
        let page = address / self.page_size;
        let line_in_page = (address % self.page_size) / self.line_size;

        //get the structure governing data for the current predictor on the relevant memory page
        let page_map = match self.map.entry(page) {
            Entry::Occupied(entry) => {
                entry.into_mut()
            },
            Entry::Vacant(v) => {
                v.insert(PageMap::new())
            },
        };

        //get the structure governing data for the relevant cacheline
        let line_map = match page_map.entry(line_in_page) {
            std::collections::btree_map::Entry::Occupied(entry) => {
                entry.into_mut()
            },
            std::collections::btree_map::Entry::Vacant(v) => {
                //size is effectively ceil(page_size / line_size) without decimal truncation
                v.insert([Option::None; NUM_CPUS])
            },
        };

        //get line-cpu stats
        match line_map[cpu] {
            Some(mut stats) => {
                stats.put(address, hit);
                //the value was cloned so we have to overwrite the original
                line_map[cpu] = Option::from(stats);
            },
            None => {
                let mut stats = LineCpuStats::new();
                stats.put(address, hit);
                line_map[cpu] = Option::from(stats);
            },
        }

        Ok(())
    }

    pub fn get_overall_stats(&self, address_range: Range<u64>, cpus: [bool; NUM_CPUS]) -> (u64, u64, u64, u64) {
        let mut stats = (0, 0, 0, 0); // (hits, possible, extraneous, issued)
        for (page_index, page_map) in self.map.iter() {
            //select pages where at least some of the page is in the specified range
            let page_start_addr = *page_index * self.page_size;
            if page_start_addr + self.page_size > address_range.start && page_start_addr < address_range.end {
                for (line_in_page, line_map) in page_map.iter() {
                    // select lines where the start address is in the specified range
                    // (address range is only accurate to cacheline precision, because we don't
                    // store exact addresses for all the previous accesses)
                    let line_start_addr = page_start_addr + (*line_in_page * self.line_size);
                    if line_start_addr >= address_range.start && line_start_addr < address_range.end {
                        for i in 0..line_map.len() {
                            if cpus[i] {
                                match line_map[i] {
                                    Some(line_cpu_stats) => {
                                        stats.0 += line_cpu_stats.hits;
                                        stats.1 += line_cpu_stats.possible;
                                        stats.2 += line_cpu_stats.extraneous;
                                        stats.3 += line_cpu_stats.issued;
                                    },
                                    None => {}
                                }
                            }
                        }
                    }
                }
            }
        }
        stats
    }
}

pub struct Prediction {
    pub time_step: u64,
    pub cpu: u8,
    pub predictor: Predictor,
    pub address: u64,
    pub hit: Outcome,
    //values used only for ACBias
    pub target: u64,
    pub register_values: Option<[u64; NUM_REGISTERS]>
}

/*pub fn filter_predictions(predictions: &HashMap<Predictor, Vec<Prediction>>, predictor: Predictor, address_range: Range<u64>, cpus: [bool; NUM_CPUS]) -> Vec<&Prediction> {
    let opt_list: Option<&Vec<Prediction>> = predictions.get(&predictor);
    match opt_list {
        Some(list) => {
            list.iter().filter(|prediction| -> bool {
                address_range.contains(&prediction.address) && cpus[prediction.cpu as usize]
            }).collect()
        }
        None => {
            Vec::new()
        }
    }
}*/

#[derive(Copy)]
#[derive(Clone)]
pub enum Outcome {
    Miss, //Cache misses (values that we had to fetch from DRAM) or incorrect branch predictions
    Hit,  //Cache hits or correct branch predictions
    Extraneous, //Indicates a useless prefetch (one that was evicted without ever being hit)
    Issued,    //Indicates a prefetch that has been issued but has not caused a hit or been evicted yet.
}