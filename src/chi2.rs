use std::collections::HashMap;
use std::collections::hash_map::Entry;
use mathru::statistics::distrib::{ChiSquared, Continuous};

//chi^2 test of independence
pub fn chi2_two_way(matrix: Vec<Vec<(&u64, &u64)>>) -> f64 {
    let mut value_column_indices_sizes: HashMap<u64, (u64, u64)> = HashMap::new();
    let mut columns = 0;
    let rows = matrix.len();
    if rows < 2 {
        // We can't do a test if we don't have more than one value of the register. 
        // By convention we say that has zero bias (p value of 1)
        return 1.0;
    }
    let mut actual_counts: Vec<Vec<u64>> = vec![Vec::new(); rows];
    let mut overall_total = 0u64;
    let mut row_totals = vec![0u64; rows];
    //calculate actual counts and row/column totals
    for row in 0..rows {
        for (value, count) in &matrix[row] {
            match value_column_indices_sizes.entry(**value) {
                Entry::Occupied(mut entry) => {
                    let (column, column_total) = entry.get_mut();
                    pad(&mut actual_counts[row], *column + 1);
                    actual_counts[row][*column as usize] = **count;
                    *column_total += **count;
                },
                Entry::Vacant(entry) => {
                    pad(&mut actual_counts[row], columns + 1);
                    actual_counts[row][columns as usize] = **count;
                    entry.insert((columns, **count));
                    columns += 1;
                },
            }
            row_totals[row] += **count;
            overall_total += **count;
        }
    }
    if columns < 2 {
        // We can't do a a test if we always load the same trigger address no matter what the register's value is.
        // By convention we say that situation has zero bias (p value of 1)
        return 1.0; 
    }

    //calculate expected counts and chi^2
    let mut chi2 = 0.0;

    for row in 0..rows {
        for (value, count) in &matrix[row] {
            match value_column_indices_sizes.entry(**value) {
                Entry::Occupied(mut entry) => {
                    let (column, column_total) = entry.get_mut();
                    let expected_count = *column_total as f64 * row_totals[row] as f64 / overall_total as f64;
                    chi2 += f64::powi(actual_counts[row][(*column) as usize] as f64 - expected_count, 2) / expected_count;
                },
                Entry::Vacant(entry) => {
                    panic!("Should never get here!");
                },
            }
        }
    }

    let p_val = 1.0 - ChiSquared::<f64>::new((rows as u32 - 1) * (columns as u32 - 1)).cdf(chi2);
    p_val

}

fn pad(vec: &mut Vec<u64>, length: u64) {
    if length > vec.len() as u64 {
        for _i in 0..(length - vec.len() as u64) {
            vec.push(0);
        }
    }
}