use crate::registers::Register::*;

pub const NUM_REGISTERS: usize = 17;

#[derive(Clone, Copy)]
pub enum Register {
    RDI,
    RSI,
    RAX,
    RBX,
    RCX,
    RDX,
    RSP,
    RBP,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    RIP
}

impl Register {
    pub fn to_str(&self) -> &str {
        match self {
            RDI => {"RDI"},
            RSI => {"RSI"},
            RAX => {"RAX"},
            RBX => {"RBX"},
            RCX => {"RCX"},
            RDX => {"RDX"},
            RSP => {"RSP"},
            RBP => {"RBP"},
            R8 => {"R8"},
            R9 => {"R9"},
            R10 => {"R10"},
            R11 => {"R11"},
            R12 => {"R12"},
            R13 => {"R13"},
            R14 => {"R14"},
            R15 => {"R15"},
            RIP => {"RIP"},
        }
    }
}

pub const REGISTERS: [Register; 17] = [
    RDI,
    RSI,
    RAX,
    RBX,
    RCX,
    RDX,
    RSP,
    RBP,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    RIP
];