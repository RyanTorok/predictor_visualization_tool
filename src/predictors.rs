
pub const NUM_CPUS: usize = 16;

#[derive(Hash)]
#[derive(PartialEq)]
#[derive(Eq)]
pub enum Predictor {
    L1ICache,
    L1DCache,
    L2Cache,
    L3Cache,
    L1IPrefetcher,
    L1DPrefetcher,
    L2Prefetcher,
    L3Prefetcher,
    Branch,
    ACBias
}

#[derive(Hash)]
#[derive(PartialEq)]
#[derive(Eq)]
pub enum PredictorClass {
    Cache,
    Prefetcher,
    Branch,
    ACBias
}

