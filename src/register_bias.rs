use crate::registers::{Register, NUM_REGISTERS};
use std::collections::HashMap;
use std::collections::hash_map::{Entry};
use std::process::Command;
use crate::chi2::chi2_two_way;

type PcSourceRegisters = [Option<(Register, u64, u64)>; 3]; //Register, value, Number of L1D accesses since value set
struct PcInstanceStats {
    source_registers: PcSourceRegisters
}

type PcStats = Vec<PcInstanceStats>;

type ValueBias = HashMap<u64, u64>; //list of prefetch address with particular frequency
type RegisterBias = HashMap<u64, ValueBias>; //map of register values to set of prefetch address/frequency
type RegisterBiases = [Option<RegisterBias>; NUM_REGISTERS];
pub struct ACBiasStats {
    map: HashMap<u64, RegisterBiases>
}

impl ACBiasStats {

    pub fn new() -> ACBiasStats {
        ACBiasStats {
            map: HashMap::new()
        }
    }

    pub fn put(&mut self, trigger: u64, target: u64, registers: [u64; NUM_REGISTERS]) {
        let register_biases = match self.map.entry(trigger) {
            Entry::Occupied(entry) => {
                entry.into_mut()
            },
            Entry::Vacant(v) => {
                let mut new_biases : RegisterBiases = Default::default();
                for i in 0..NUM_REGISTERS {
                    new_biases[i] = Option::from(RegisterBias::new());
                }
                v.insert(new_biases)
            },
        };
        for i in 0..NUM_REGISTERS {
            let register_bias = &mut register_biases[i];
            //can unwrap here because Option::None is only used for initialization and immediately overwritten.
            let mut value_bias = match register_bias.as_mut().unwrap().entry(registers[i]) {
                Entry::Occupied(entry) => {
                    entry.into_mut()
                },
                Entry::Vacant(v) => {
                    v.insert(HashMap::new())
                },
            };
            let mut found = false;
            let ref_value_bias = &mut value_bias;
            for entry in ref_value_bias.iter_mut() {
                if *(entry.0) == target {
                    found = true;
                    *(entry.1) += 1;
                }
            }
            if !found {
                value_bias.insert(target, 1);
            }
        }
    }

    pub fn overall_bias(&self, r: Register) {
        let mut bias: f64 = 0.0;
        for entry in self.map.iter() {
            bias += match entry.1[r as usize] {
                Some(_) => {
                    1.0
                },
                None => {0.0},
            };
        }
    }

    pub fn trigger_bias(&self, r: Register, trigger: u64) -> f64 {
        match self.map.get(&trigger) {
            Some(register_biases) => {
                match register_biases[r as usize].as_ref() {
                    Some(register_bias) => {
                        let mut matrix: Vec<Vec<(&u64, &u64)>> = vec![];
                        for (register_value, value) in register_bias.iter() {
                            let mut row: Vec<(&u64, &u64)> = vec![];
                            for target in value {
                                row.push(target);
                            }
                            matrix.push(row);
                        }
                        1.0 - chi2_two_way(matrix)
                    },
                    None => {0.0},
                }
            },
            None => {0.0},
        }
    }

}

